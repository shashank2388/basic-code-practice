# -*- coding: utf-8 -*-
# @Author: Shashank Tupkar
# @Date:   2020-12-13 10:01:38
# @Last Modified time: 2020-12-14 09:22:37

# I have created seperate functions for each problem.


"""
Function to check whether Names follow the format.
name: ensure_name
parameters: list of NAMES
parameter type: list
return: True or False

"""
def ensure_name(NAMES):
    for name in NAMES:
        name_format = name.split("_")
        if (len(name_format) < 3):
            return False;
        elif (("test" != name_format[0]) or (name_format[2].isdigit() == False)):
            return False
    return True


"""
Function to ensure integer ID is great than 0.
name: ensure_id
parameters: list of NAMES
parameter type: list
return: True or False

"""
def ensure_id(NAMES):
    for name in NAMES:
        name_format = name.split("_")
        if (len(name_format) < 3):
            return False;
        elif (name_format[2].isdigit() == False) or (int(name_format[2]) < 0):
            return False
    return True


"""
Function to ensure there are no duplicate names.
name: ensure_unique_names
parameters: list of NAMES
parameter type: list
return: True or False

"""
def ensure_unique_names(NAMES):
    unique_list = []
    for name in NAMES:
        if name not in unique_list:
            unique_list.append(name)
        else:
            return False
    return True



def main():
    NAMES = [
        "test_log_ger_2",
        "test_logger_1_optional_suffix",
        "test_logger_-1_another_optional_suffix",
        "test_updater_4",
        "test_updater_1",
        "",
    ]

    ensure_name(NAMES)
    ensure_id(NAMES)
    ensure_unique_names(NAMES)

main()
