# -*- coding: utf-8 -*-
# @Author: Shashank Tupkar
# @Date:   2020-12-13 11:51:38
# @Last Modified time: 2020-12-14 09:22:18


import unittest
from reverse import reserve_2d_array

"""
Function to run unit test cases for the main reserve 2d array.

"""
class Test_2d_array(unittest.TestCase):

    # Test Case 1: To check if both the inputs are integers
    # Assumption - Function raises value error if input values are not integers.
    def test_integer_inputs(self):
        self.assertRaises(ValueError, reserve_2d_array, 1.5, 0.3)

    # Test Case 2: To check if function raises error if both the inputs are less than zero
    def test_valueError_rows(self):
        self.assertRaises(ValueError, reserve_2d_array, -1, 0)

    def test_valueError_cols(self):
        self.assertRaises(ValueError, reserve_2d_array, 0, -1)

    # Test Case 3: To check if the inputs are zero
    # Assumption - Function would return empty list.
    def test_valueError_rows(self):
        actual_value = reserve_2d_array(0, 0)
        expected_value = []
        self.assertEqual(actual_value, expected_value)

    # Test Case 4: To check if both the inputs are provided
    # Assumption - Function raises type error if input values are none.
    def test_valueError_none_inputs(self):
        self.assertRaises(TypeError, reserve_2d_array, None, None)

    # Test Case 5: To check if the function outputs correctly
    def test_reserve_2d_array(self):
        actual_value = reserve_2d_array(2, 3)
        expected_value = [
                            [0x00, 0x00, 0x00],
                            [0x00, 0x00, 0x00]
                         ]
        self.assertEqual(actual_value, expected_value)

    # Test Case 6: To check if the number of rows and cols are equal to inputs
    def test_result_rows_cols(self):
        result = reserve_2d_array(1, 3)
        expected_rows = 1
        expected_columns = 3
        self.assertEqual(len(result), expected_rows)
        self.assertEqual(len(result[0]), expected_columns)

    # Test Case 7: To check if all the elements are bytes
    # Assumption - Function returns bytearray
    def test_result_bytes(self):
        result = reserve_2d_array(1, 2)
        result_type = type(result)
        expected_type = True
        self.assertEqual(type(result) is bytearray, expected_type)


if __name__ == '__main__':
    unittest.main()

