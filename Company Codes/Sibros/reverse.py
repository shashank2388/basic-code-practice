from typing import List

def reserve_2d_array(rows: int, columns: int) -> List[bytes]:
    """
    Create a list of bytes given a number of rows and a number of columns
    where each element of the 2D array is zero

    For example, given rows = 2, columns = 3:
    [
        [0x00, 0x00, 0x00],
        [0x00, 0x00, 0x00]
    ]

    :raise: ValueError if rows or columns is less than zero
    """
    # Assume another developer will provide implementation

    byte_list = [
        [0x00, 0x00, 0x00]
    ]
    str = "Geeksforgeeks"
    array1 = bytes(str, 'utf-8')

    print(type(array1) is bytes)

    if (rows == None) or (columns == None):
        raise TypeError("No value provided")

    if (rows < 0) or (columns < 0):
        raise ValueError("Rows or Cols is < 0")


    # try:
    if not isinstance(rows, int) or not isinstance(columns, int):
        raise ValueError("Error")

# str = "Geeksforgeeks"

# array1 = bytearray(str, 'utf-8')
# print(array1)
# print(array1.decode())

    return byte_list


def main():
    reserve_2d_array(0.5, 2)

main()