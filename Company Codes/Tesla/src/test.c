///*
// ============================================================================
// Name        : test.c
// Author      : Shashank
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
// ============================================================================
// */
//
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#define SECS_NON_LEAP_YEAR (60L*60L*24L*365UL)

#define MIN(x,y) ({ 							\
						typeof (x) _x = (x);	\
						typeof (y) _y = (y); 	\
						_x < _y ? _x : _y; 		\
				  })

#define C_TO_F(x) ({                         \
                      typeof (x) _x = (x);   \
                      _x * (9.0/5.0) + 32;   \
                    })


#define B2(n) 		n,     n+1,     n+1,     n+2
#define B4(n) 	B2(n), B2(n+1), B2(n+1), B2(n+2)
#define B6(n) 	B4(n), B4(n+1), B4(n+1), B4(n+2)

#define set_look_up B6(0), B6(1), B6(1), B6(2)


uint16_t count_bit_using_lookup (uint32_t v)
{
	unsigned int lookuptable[256] = { set_look_up };
	uint16_t c; // c is the total bits set in v

	c =	lookuptable[v & 0xff] + lookuptable[(v >> 8) & 0xff] +
		lookuptable[(v >> 16) & 0xff] + lookuptable[v >> 24];

	return c;
}


int square (volatile int *ptr)
{
	int a;
	a = *ptr;
	return a * a;
}


typedef enum states
{
	state_A,
	state_B,
	state_C,
	state_Fault,
	max_states
}State_t;

typedef enum inputs
{
	input_1 = 1,
	input_2,
	input_3,
	input_4,
	input_5,
	input_6,
	max_inputs
}Input_t;

State_t execute_state_A ()
{
	printf("State A \n");
	return state_A;
}

State_t execute_state_B ()
{
	printf("State B \n");
	return state_B;
}

State_t execute_state_C ()
{
	printf("State C \n");
	return state_C;
}

State_t execute_state_Faulty ()
{
	printf("Fault \n");
	return state_Fault;
}

Input_t get_input()
{
	int x = rand() % 10;
	printf("%d \n", x);
	return x;
}

int main(void)
{

//	int a = 255;
//	printf("%d \n", count_bit_using_lookup(a));
//
//	int b = 5;
//	char d ='D';
//	printf("%d \n", MIN(a, b));
//	printf("%d \n", MIN(a, d));

	printf("%f", C_TO_F(5));


	volatile int c;

	long x = square(&c);
//
//	int *p = (int *)0x1000;
//	*p = 0x1234;

	State_t state = state_A;
	State_t next_state;
	Input_t input;
	while(1)
	{
		input = get_input();

//		sleep(2);
		switch (state)
		{
			case state_A:
				printf("State_A ->");
				switch (input)
				{
					case input_3:
						next_state = execute_state_B();
						break;

					case input_2:
						next_state = execute_state_C();
						break;

					case input_1:
					case input_4:
					case input_5:
					case input_6:
						break;

					default:
						next_state = execute_state_Faulty();
						break;
				}
				break;

			case state_B:
				printf("State_B ->");
				switch (input)
				{
					case input_5:
						next_state = execute_state_C();
						break;

					case input_4:
						next_state = execute_state_A();
						break;

					case input_1:
					case input_2:
					case input_3:
					case input_6:
						break;

					default:
						next_state = execute_state_Faulty();
						break;
				}
				break;

			case state_C:
				printf("State_C ->");
				switch (input)
				{
					case input_6:
						next_state = execute_state_B();
						break;
					case input_1:
						next_state = execute_state_A();
						break;

					case input_2:
					case input_3:
					case input_4:
					case input_5:
						break;

					default:
						next_state = execute_state_Faulty();
						break;
				}
				break;

			case state_Fault:
				return EXIT_SUCCESS;
				break;

			default:
				next_state = execute_state_Faulty();
				break;

		}
		state = next_state;
	}

	return EXIT_SUCCESS;
}

