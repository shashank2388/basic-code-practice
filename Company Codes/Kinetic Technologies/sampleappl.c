/**
 *  Kinetics Sample Question
 *
 *  @brief   Task and ISR Templates
 *  @notes   The Tasks are already assumed to created and ready to run.
 *           
 */

/* Add any other includes you believe you need here */
#include "rtosapi.h"               /* Other  example RTOS APIs */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/**
 *
 * All REGISTERS are 32-bits and word aligned
 * Atomic 8-, 16- and 32-bit accesses are supported
 *
 */

/**
 * These Register_u8, Register_u16, and Register_u32 typedefs need to be implemented
 */
//typedef struct
//{
//  union {
//    volatile uint32_t Register_u32;
//    struct {
//    	volatile uint16_t Register_u16_1;
//    	volatile uint16_t Register_u16_2;
//    };
//    struct {
//    	volatile uint8_t  Register_u8_1;
//    	volatile uint8_t  Register_u8_2;
//    	volatile uint8_t  Register_u8_3;
//    	volatile uint8_t  Register_u8_4;
//    };
//  } Reg;
//} MU_Register_t;

//To access use 	MU_Register_t_object.Reg_object.Register_u32

typedef volatile uint8_t	Register_u8;
typedef volatile uint16_t 	Register_u16;
typedef volatile uint32_t 	Register_u32;

#define RX_BUFFER_REGISTER_ADDRESS			(0x1000)
#define RX_BUFFER_REGISTER_ADDRESS_END      (0x1031)
#define TX_BUFFER_REGISTER_ADDRESS	        (0x300)

#define INTERRUPT_CONTROL_REGISTER_ADDRESS	(0x204)
#define INTERRUPT_STATUS_REGISTER_ADDRESS	(0x208)
//Bit to enable or disable interrupt
#define INTERRUPT_CONTROL_REG_ENABLE_DISABLE_BIT         4

//Bit for the status of interrupt
#define INTERRUPT_STATUS_REG_CLR_BIT          			4

#define TX_INITIATE_REGISTER_ADDRESS		(0x500)

/**
 * These buffers are memory mapped and accessed at 8-bit
 */
#define VIDEO_DATA_STORE_ADDRESS			(0x2000)
#define VIDEO_DATA_STORE_STATUS				(0x2004)

#define TRANSMIT_START_MESSAGE				0x1
#define TX_TIMER_INTERVAL_MS				20
#define TIMER_EXPIRED_EVENT_FLAG            0x1

/**
 * These macros need to be implemented
 */
#define GET_BIT(Value, BitPosition) 	((((Register_u32)(Value)) & \
										((Register_u32)(1 << BitPosition))) >> BitPosition)
#define SET_BIT(Value, BitPosition) 	((*(Register_u32*)Value) |= ((Register_u32)(1 << BitPosition)))
#define CLEAR_BIT(Value, BitPosition) 	((*(Register_u32*)Value) &= ~((Register_u32)(1 << BitPosition)))


/**
 *  Event flags Bit masks
 */
#define VIDEO_DATA_RECEIVED_EVENT_FLAG		0x1
#define VIDEO_DATA_0_7_NON_ZERO_EVENT_FLAG	0x2
#define VIDEO_DATA_8_27_NON_ZERO_EVENT_FLAG	0x4



/**
 * RX and TX Data buffer declarations
 */
/* Insert your RxDataBuffer and TxDataBuffer declarations here */

typedef struct {
	uint8_t header[4];
	uint8_t payload[28];
} RxDataBuffer;

typedef struct {
	uint8_t header[4];
	uint8_t data[20];
} TxDataBuffer;

typedef struct {
	uint8_t data[8];
} TxDataProcessBuffer;

RxDataBuffer 			VideoDataRxBuffer;
TxDataBuffer 			VideoDataTxBuffer;
TxDataProcessBuffer 	VideoDpBuffer;


/*
uint32_t MessageQId;
uint32_t EventId;
 */
uint32_t TransmitterTaskMessageQId;
uint32_t ReceiverTask_EventId;
uint32_t DataProcessorTask_EventId;
uint32_t TransmitterTask_EventId;
uint32_t Timer_EventId;

// Event names
const char Rx_Evt_Id_Des[]    = "Receiver Task Event";
const char Tx_Evt_Id_Des[]    = "Transmitter Task Event";
const char Dp_Evt_Id_Des[]    = "Data Processor Task Event";
const char Tim_Evt_Id_Des[]   = "Timer Event";
const char Tx_Queue_Id_Des[]  = "TX Queue";

uint32_t TxTaskTimerHandle;

void TransmitterDataHandler(uint32_t TimerId);
void ReceivedDataHandler(void);

/**
 * @brief ReceiverISRHandler
 * @fn    void ReceiverISRHandler (void)
 * @note  This ISR handler is assumed to already be assigned to the correct Interrupt level
 *        and number
 */
void ReceiverISRHandler(void *TaskParams)
{
	/* Disable the interrupt. */
	CLEAR_BIT(INTERRUPT_CONTROL_REGISTER_ADDRESS, INTERRUPT_CONTROL_REG_ENABLE_DISABLE_BIT);
	/* Clear the interrupt so that it can fire again when it is re-enabled. */
	CLEAR_BIT(INTERRUPT_STATUS_REGISTER_ADDRESS, INTERRUPT_STATUS_REG_CLR_BIT);
	/* Check if ReceiverTask is ready to handle Received data */
	ReceivedDataHandler();
	/* Enable the interrupt. */
	SET_BIT(INTERRUPT_CONTROL_REGISTER_ADDRESS, INTERRUPT_CONTROL_REG_ENABLE_DISABLE_BIT);
}

void ReceivedDataHandler(void)
{

	uint32_t rxFlags = 0;

	/*
	 * Since data is in Little Endian format, 0x1000 has the MSB.
	 * Each 32-bit HW address location will hold 8-bit data (1-byte).
	 * There are 32-bytes of data - 4 byte header and 28 byte payload.
	 * 0x1031 will contain the start of the packet.
	 */
	Register_u32* ptrHwAddr = (Register_u32*)RX_BUFFER_REGISTER_ADDRESS_END;

	/* Check if ReceiverTask Flag is clear.
	 * If ReceiverTask is not ready to process this data, there is no point in copying the data.
	 */
	RTOS_GetEvent(ReceiverTask_EventId, &rxFlags);

	if ((GET_BIT(rxFlags, VIDEO_DATA_RECEIVED_EVENT_FLAG)) == 0) {

		/* Copy Header */
		for(int i=0; i<4; i++) {
			VideoDataRxBuffer.header[i] = (uint8_t)(*ptrHwAddr);
			ptrHwAddr--;
		}

		/* Copy Payload */
		for(int i=0; i<28; i++) {
			VideoDataRxBuffer.payload[i] = (uint8_t)(*ptrHwAddr);
			ptrHwAddr--;
		}
	}

	/* Data has been copied from HW, let ReceiverTask know about it. */
	RTOS_SetEvent(ReceiverTask_EventId, VIDEO_DATA_RECEIVED_EVENT_FLAG);

}

/**
 * @brief ReceiverTask -
 * @fn    void ReceiverTask (void *TaskParams)
 * @param[in] TaskParams - not used
 * @note  
 */
void ReceiverTask(void *TaskParams)
{
	/* Local variables for use */
	uint32_t flagRes = 0;
	uint32_t dpFlags = 0;
	uint32_t txFlags = 0;

	Register_u8 inputData = 0;

	TxDataBuffer localVideoDpBuffer;

	while (1)
	{
		/* Wait indefinitely until ISR sets the event flag. */
		RTOS_WaitEvent(ReceiverTask_EventId, VIDEO_DATA_RECEIVED_EVENT_FLAG, &flagRes, RTOS_EVENT_OR, RTOS_PEND);

		/* Check if DataProcessorTask is ready to accept new data */
		RTOS_GetEvent(DataProcessorTask_EventId, &dpFlags);

		if (GET_BIT(dpFlags, VIDEO_DATA_0_7_NON_ZERO_EVENT_FLAG) == 0) {

			inputData = 0;

			for(int i=0; i<8; i++) {
				inputData |= VideoDataRxBuffer.payload[i];
			}

			/* If there is non-zero input data in first 8 bytes, then send it to DataProcessorTask for processing */
			if (inputData != 0) {
				memcpy(VideoDpBuffer.data, VideoDataRxBuffer.payload, 8);
				/* Set flag to let DataProcessorTask know about new data */
				RTOS_SetEvent(DataProcessorTask_EventId, VIDEO_DATA_0_7_NON_ZERO_EVENT_FLAG);
			}
		}

		/* Check if TransmitterTask is ready to accept new data. */
		RTOS_GetEvent(TransmitterTask_EventId, &txFlags);

		if (GET_BIT(txFlags, VIDEO_DATA_8_27_NON_ZERO_EVENT_FLAG) == 0) {
			inputData = 0;
			for(int i=8; i<28; i++) {
				inputData |= VideoDataRxBuffer.payload[i];
			}
			/* If there is non-zero input data in last 20 bytes, then send it to TransmitterTask for processing */
			if (inputData != 0) {
				memcpy(localVideoDpBuffer.header, VideoDataRxBuffer.header, 4);
				memcpy(localVideoDpBuffer.data, &VideoDataRxBuffer.payload[8], 20);

				RTOS_PutQueueData(TransmitterTaskMessageQId, &localVideoDpBuffer, sizeof(localVideoDpBuffer));

				RTOS_SetEvent(TransmitterTask_EventId, VIDEO_DATA_8_27_NON_ZERO_EVENT_FLAG);
			}
		}

		/* Clear flag so that ISR knows we are ready */
		RTOS_ClearEvent(ReceiverTask_EventId, VIDEO_DATA_RECEIVED_EVENT_FLAG);
	}
}

/**
 * Callback for the Timer
 */
void TransmitterDataHandler(uint32_t TimerId)
{
	/* If the bit is clear, then set the flag to allow TransmitterTask to progress */
	if(GET_BIT(TX_INITIATE_REGISTER_ADDRESS, 3) == 0) {
		RTOS_SetEvent(Timer_EventId, TIMER_EXPIRED_EVENT_FLAG);
	}

	/* If the bit hasn't cleared yet, re-start the timer again. */
	else {
		RTOS_StartTimer(&TxTaskTimerHandle, TX_TIMER_INTERVAL_MS, 0, 0, TransmitterDataHandler, 0, 0);
	}
}

/**
 * @brief TransmitterTask -
 * @fn    void TransmitterTask (void *TaskParams) 
 * @param[in] TaskParams - not used
 * @note  
 */
void TransmitterTask(void *TaskParams)
{
	/* Local variables for use */
	uint32_t flagRes = 0;
	uint32_t timFlagRes = 0;
	TxDataBuffer localTxDataBuffer;

	// Concatenated data 4-byte header + 20-byte payload
	uint8_t concatData[24];

	// Current index of what needs to be transmitted next to HW
	uint32_t concatIndex = 0;

	while (1)
	{
		/* Wait indefinitely until event flag is set by ReceiverTask */
		RTOS_WaitEvent(TransmitterTask_EventId, VIDEO_DATA_8_27_NON_ZERO_EVENT_FLAG, &flagRes, RTOS_EVENT_OR, RTOS_PEND);

		/* Copy data from queue. */
		RTOS_GetQueueData(TransmitterTaskMessageQId, &localTxDataBuffer, sizeof(TxDataBuffer), RTOS_PEND);

		/* Copy data into an array */
		memcpy(concatData, localTxDataBuffer.header, 4);
		memcpy(&concatData[4], localTxDataBuffer.data, 20);
		concatIndex = 0;

		do {
			/* Write data to HW address */
			(*((Register_u32 *)TX_BUFFER_REGISTER_ADDRESS)) = concatData[concatIndex];

			/* Set bit to initiate transmission */
			SET_BIT(TX_INITIATE_REGISTER_ADDRESS, 3);

			/* Start timer for 20ms */
			RTOS_StartTimer(&TxTaskTimerHandle, TX_TIMER_INTERVAL_MS, 0, 0, TransmitterDataHandler, 0, 0);

			/* Wait indefinitely on timer flag */
			RTOS_WaitEvent(Timer_EventId, TIMER_EXPIRED_EVENT_FLAG, &timFlagRes, RTOS_EVENT_OR_CLR, RTOS_PEND);
			concatIndex++;
		} while(concatIndex < 24);

		/* We are done processing this message, let the ReceiverTask know we are ready for the next message */
		RTOS_ClearEvent(TransmitterTask_EventId, VIDEO_DATA_8_27_NON_ZERO_EVENT_FLAG);
	}
}

void DataProcessDataHandler(void)
{
}

/**
 * @brief DataProcessorTask -
 * @fn    void DataProcessorTask (void *TaskParams) 
 * @param[in] TaskParams - not used
 * @note  
 */
void DataProcessorTask(void *TaskParams)
{
	/* Local variables for use */
	uint32_t flagRes;
	uint8_t dataResult = 0;

	while (1)
	{
		/* Wait indefinitely until event flag is set by ReceiverTask */
		RTOS_WaitEvent(DataProcessorTask_EventId, VIDEO_DATA_0_7_NON_ZERO_EVENT_FLAG, &flagRes, RTOS_EVENT_OR, RTOS_PEND);

		/* Process the message only if the other task is ready for it. The other task will clear this bit when it has consumed the data */
		if(GET_BIT(VIDEO_DATA_STORE_STATUS, 5) == 0) {
			/* Process the message */
			dataResult = 0;
			dataResult |= VideoDpBuffer.data[0];
			dataResult |= VideoDpBuffer.data[3];
			dataResult |= VideoDpBuffer.data[7];

			/* Write data to HW Register */
			(*((Register_u32 *)VIDEO_DATA_STORE_ADDRESS)) = dataResult;

			/* Set Bit 5 to indicate data has been updated */
			SET_BIT(VIDEO_DATA_STORE_STATUS, 5);
		}

		/* Irrespective of whether the message was used by this task or not,
	    clear the event flag so that a new message can arrive. */
		RTOS_ClearEvent(DataProcessorTask_EventId, VIDEO_DATA_0_7_NON_ZERO_EVENT_FLAG);
	}
}

/**
 * @brief Application Initialization
 * @fn    void ApplicationInitialization()
 * @note  
 */
void ApplicationInitialization()
{
	/* Add any initializations here */
	//GPOIS initialization goes here

	/* Interrupt setup        */

	//Create events
	RTOS_CreateEvent(&ReceiverTask_EventId, Rx_Evt_Id_Des);
	RTOS_CreateEvent(&DataProcessorTask_EventId, Dp_Evt_Id_Des);
	RTOS_CreateEvent(&TransmitterTask_EventId, Tx_Evt_Id_Des);
	RTOS_CreateEvent(&Timer_EventId, Tim_Evt_Id_Des);

	//Create Queue For Transmitting messages
	RTOS_CreateQueue(&TransmitterTaskMessageQId, Tx_Queue_Id_Des, &VideoDataTxBuffer, 1, sizeof(TxDataBuffer));

}

/**
 *   @brief: main entry point
 *   @mote:  ISR, Tasks creation, OS details are all assumed 
 *           to have happened
 */
int main (void)
{
	ApplicationInitialization();

	/* Create and run Tasks   */
	//create_task (DataProcessorTask, bytes_for_memory, priority, NULL)
	//create_task (TransmitterTask, bytes_for_memory, priority, NULL)
	//create_task (ReceiverTask, bytes_for_memory, priority, NULL)

	/* Start operating system */
	//Scheduler start

}

