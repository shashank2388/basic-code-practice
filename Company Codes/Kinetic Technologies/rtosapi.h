/*
 *  Smart Connectivity Sample 
 */
#ifndef __RTOSAPI_H__
#define __RTOSAPI_H__

#include <stdint.h>


/**
 * RTOS  Error codes and other constants
 */
#define RTOS_PEND              0   /* A wait is infinite               */
#define RTOS_CHECK             1   /* Check if a Message Queue has data, return immediately  if none */

#define RTOS_EVENT_OR         (1 << 0)
#define RTOS_EVENT_AND        (1 << 1)
#define RTOS_EVENT_OR_CLR     (1 << 2)
#define RTOS_EVENT_AND_CLR    (1 << 3)

#define RTOS_NO_MESSAGE       0xbeef  /* No message to send             */

#define RTOS_SUCCESS           0   /* RTOS operation returns success    */
#define RTOS_FAILURE          -1   /* RTOS operation was not successful */

#define RTOS_QUEUE_TIMEOUT   -16   /* Queue operation has timed out     */
#define RTOS_EVENT_TIMEOUT   -17   /* Event operation has timed out     */

/**
 *  @brief      Create an RTOS Event
 *  @fn         int32_t RTOS_CreateEvent(uint32_t *EventId, const char *EventName);  
 *  @param[out] *EventId  - unique handle for Event
 *  @param[in]  EventName - unique name for this Event
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 */
int32_t RTOS_CreateEvent(uint32_t *EventId, const char *EventName );  

/**
 *  @brief      Set an Event bit(s)
 *  @fn         int32_t RTOS_SetEvent   (uint32_t EventId, uint32_t EventFlags);                   
 *  @param[in]  EventId  - unique handle for Event
 *  @param[in]  EventFlags - Set Event flags 
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 *  @note       Used to set a Event Bit in the EventFlags, this will cause any Task waiting on an RTOS_WaitEvent() 
 *              to become runnable. This call is non-blocking and will return after setting the Event(s). 
 */
int32_t RTOS_SetEvent   (uint32_t EventId, uint32_t EventFlags );                   

/**
 *  @brief      Clear an Event bit(s)
 *  @fn         int32_t RTOS_ClearEvent (uint32_t EventId, uint32_t EventFlags);     
 *  @param[in]  EventId  - unique handle for Event
 *  @param[in]  EventFlags - Clear an Event flag(s)
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 * Used to clear an Event Bit(s) after an RTOS_WaitEvent operation
 */
int32_t RTOS_ClearEvent (uint32_t EventId, uint32_t EventFlags);                   

/**
 *  @brief      Wait on an Event
 *  @fn         int32_t RTOS_WaitEvent  (uint32_t EventId, uint32_t WaitFlags, uint32_t *WaitActualFlags, 
                                         uint32_t WaitOptions, uint32_t WaitTime);
 *  @param[in]  EventId  - unique handle for Event
 *  @param[in]  WaitFlags - Event flag(s) to wait on
 *  @param[out] *WaitActualFlags - Value of actual Event flag(s) set
 *  @param[in]  WaitOptions  - See @note below
 *  @param[in]  WaitTime     - Value of TimeOut, if RTOS_PEND is passed then timeout is infinite. Othervalues are 
 *                             considered to be Timeouts in MSEC.
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 *              RTOS_EVENT_TIMEOUT - tells us that the Timeout value expired before an Event arrived
 *  @note       WaitOptions 
 *              RTOS_EVENT_OR      - all Bits in the Event WaitFlags  are OR'd together
 *              RTOS_EVENT_AND     - all Bits in the Event WaitFlags  are AND'd together
 *              RTOS_EVENT_OR_CLR  - all Bits in the Event Waitfalgfs are OR'd together and cleared afterwards
 *              RTOS_EVENT_AND_CLR - all Bits in the Event WaitFlags  are AND'd together and cleared afterwards
 * Used to wait on an Event Bit(s). This is a BLOCKING CALL
 */
int32_t RTOS_WaitEvent  (uint32_t EventId, uint32_t WaitFlags, uint32_t *WaitActualFlags, uint32_t WaitOptions, uint32_t WaitTime);

/**
 * @brief       Get an Event
 * @fn		int32_tRTOS_GetEvent (uint32_t_EventId, unit32_t *EventFlags)
 * @param[in]   EventId          Event Handle
 * @param[out]  *EventFlags      The Event flags for this event
 * @return      RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 * @note        This retrieves the current value of the Event flag for this Event. This call is non-Blocking and will return 
 *              immediately. 
 */
int32_t RTOS_GetEvent (uint32_t EventId, uint32_t *EventFlags);

/**
 * @brief       Create a Queue
 * @fn			int32_t RTOS_CreateQueue (uint32_t   *QueueId, const char *QueueName, void *QueueData, uint32_t QueueDepth, uint32_t DataSize)
 * @param[out]  *QueueId        Created Queue handle
 * @param[in]   *QueueName      Name of this Queue
 * @param[in]   *QueueData      Queue data
 * @param[in]   QueueDepth     	Queue depth
 * @param[in]   DataSize       	Size of data element
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 * @note        Create a Queue
 */
int32_t RTOS_CreateQueue (uint32_t   *QueueId, const char *QueueName, void *QueueData, uint32_t QueueDepth, uint32_t DataSize);

/**
 *  @brief      Get data from a Queue
 *  @fn         int32_t RTOS_GetQueueData  (uint32_t QueueId, void *Data, uint32_t Size, int32_t TimeOut);
 *  @param[in]  QueueId  - unique handle for Queue
 *  @param[out] *Data    - message data returned
 *  @param[in]  Size     - Size of data to read back
 *  @param[in]  TimeOut  - Value of Timeout, if RTOS_PEND then timeout is infinite
 *                         if RTOS_CHECK is used then there is no timeout and this call will return immediately if there is no data
 *                         Any other value is a Timeout in MSecs.
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 *              RTOS_QUEUE_TIMEOUT - tells us that the Timeout value expired before a Message arrived
 * 				Used to get data from a message queue, this is a BLOCKING call (unless Timeout or RTOS_CHECK is passed as a timeout value)
 */
int32_t RTOS_GetQueueData  (uint32_t QueueId, void *Data, uint32_t Size, int32_t TimeOut);

/**
 *  @brief      Put data into a Queue
 *  @fn         int32_t RTOS_PutQueueData  (uint32_t QueueId, void *Data, uint32_t Size);
 *  @param[in]  QueueId  - unique handle for this Queue
 *  @param[out] *Data    - message data to send
 *  @param[in]  Size     - Size of data to send
 *  @return     RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 *  @note       Used to put data into a message queue.
 *              This is a BLOCKING call (unless there is a Timeout or RTOS_CHECK provided)
 */
int32_t RTOS_PutQueueData  (uint32_t QueueId, void *Data, uint32_t Size);

/**
 * @brief	Create and start a Timer
 * @fn		int32_t RTOS_StartTimer(uint32_t *TimerHandle, uint32_t StartMsec, uint32_t QueueId,void *QueueData,
 *                                      void (*TimerCallbackPtr)(void), uint32_t EventID, uint32_t EventFlags)
 * @param[out]  *TimerHandle         - Handle to this Anonymous Timer
 * @param[in]   IntervalMsec         - Interval time for this Timer in MSecs
 * @param[in]   QueueId              - Which queue to send a message to?
 * @param[in]   QueueData            - Item to send in message
 * @param[in]   *TimerCallbackPtr    - User supplied callback function on Timer Expiration (assuming non-NULL)
 * @param[in]   EventID  	     - User supplied Event Group                           (assuming not = ZERO)
 * @param[in]   EventFlags           - User supplied Event Flags to be set on Timer Expiration
 * @return      RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 * @note        Creates and Starts a timer.  
 *              This Timer is ONE-SHOT i.e. after it expires it is complete. It will have to be called
 *              again to re-arm it. 
 *              On expiration the Timer can:
 *              a) Send a message to a message Queue               (if message          != RTOS_NO_MESSAGE)
 *              b) Set an Event in an Event Group                  (if EventFlags       != 0)
 *              c) Call a callback with a user supplied operation  (If TimerCallbackPtr != NULL)
 */
int32_t RTOS_StartTimer(uint32_t *TimerHandle, uint32_t StartMsec, uint32_t QueueId, void *QueueData,
		        void (*TimerCallbackPtr)(uint32_t TimerId), uint32_t EventID, uint32_t EventFlags);

/**
 * @brief       Stop a timer
 * @fn	        int32_t RTOS_StopTimer (uint32_t TimerId)
 * @param[in]   TimerId            	 Handle for timer
 * @return      RTOS_SUCCESS - all went well
 *              RTOS_FAILURE - all did not go well
 */
int32_t RTOS_StopTimer          (uint32_t TimerId);


#endif
