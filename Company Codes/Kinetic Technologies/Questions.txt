* Can you explain the overall flow of your code?
Yes.. ISR handle->DataReceiverHandle

* You've statically allocated TransmitterTaskMessageQId, why? Can we use a dynamically allocated queue? Why do we not want to use that?
We can but it is provided by the RTOS and it handle all the necessary fucntions internally. dynamic will cause fragmentation

* In ReceivedDataHandler() function, you are checking for (GET_BIT(rxFlags,VIDEO_DATA_RECEIVED_EVENT_FLAG)) == 0), what will happen to the entire system, if the VIDEO_DATA_RECEIVED_EVENT_FLAG is never cleared?
If it is not cleared the VideoDataRxBuffer will not be filled and it will not trigger the  ReceiverTask_EventId which in turn triggers the other data.


* In ReceiverTask, what will happen if VIDEO_DATA_0_7_NON_ZERO_EVENT_FLAG is never cleared?
The inputdata will not be filled and the DataProcessorTask event will not be triggered.

* In ReceiverTask, what will happen if VIDEO_DATA_8_27_NON_ZERO_EVENT_FLAG is never cleared?
The localVideoDpBuffer will not be filled, no data to send on the Queue and the TransmitterTask event will not be triggered.

* In ReceiverTask, RTOS_PutQueueData is a blocking call. What do we mean by a blocking call?
It will wait until the entire data is copied in the queue from the buffer(&localVideoDpBuffer).

* Is there a chance that RTOS_PutQueueData will block inside the ReceiverTask forever? If yes, what will happen in such case when ReceiverTask is completely blocked forever on RTOS_PutQueueData. If no,
why won't ReceiverTask block forever?
Yes, the system will be blocked


* Data in this code is being shared via global structures - RxDataBuffer, TxDataBuffer and TxDataProcessBuffer. What are some other ways we can share data?
using queues

* In DataProcessorTask what will happen if the other task never clears VIDEO_DATA_STORE_STATUS? What have you chosen to do in such a case?
in that case it will not go inside the if(GET_BIT(VIDEO_DATA_STORE_STATUS, 5) == 0) loop and the value of VIDEO_DATA_STORE_ADDRESS will never be updated.


* Given the current rtosapi.h, can we use RTOS_PutQueueData inside the ISR?
No.. cos it is blocking call

** We are using 4 event flags with 4 event IDs, can we reduce it? How?
->eventids can be shared between different process. 1 eventid flag can have different events.

** You've used const char Rx_Evt_Id_Des[] how is it different from char Rx_Evt_Id_Des[]? What will C Programming do differently between these two definitions?
->Const char it will be stored in the code segment and other will stored in BSS

** You are using a flag in the Timer Callback Function TransmitterDataHandler to indicate to the TransmitterTask that 20ms are done, are there more ways to do it? Can you explain them?
We can use hardware timer interrupt
 *              On expiration the Timer can:
 *              a) Send a message to a message Queue               (if message          != RTOS_NO_MESSAGE)
 *              b) Set an Event in an Event Group                  (if EventFlags       != 0) // This we are using 
 *              c) Call a callback with a user supplied operation  (If TimerCallbackPtr != NULL)

** We've made some major changes in the code and now, the OtherTask is taking longer than expected to clear the VIDEO_DATA_STORE_STATUS. We've decided to save the latest 5 dataResult values in
DataProcessorTask, what changes do you need to make for this?
->We can implement a circular buffer
issue with circular buffer is raise condition.... 


** What priorities would you assign to the threads in this program? Explain.
->All can have same priorities or the receiver task should have higher priority and the other 2 tasks can have same or different priorities

** If HW Data that is being read in ReceiverISRHandler is changing faster than the time taken for you to copy it into VideoDataRxBuffer, can you think of what HW/SW changes need to be done to catch up to
this speed?
-> We can use DMA to speed up the copy process. DMA can be set up and start as soon as interrupt occurs.
Have a HW buffer chip to latch the data when interrupt happens so that software can copy it later at normal.


*** In ReceiverISRHandler ISR, you've disabled the interrupt so that you can read data from HW. Does that mean data in HW won't change? If it can change, what is your assumption here?
->yes the assumption is data in hardware buffer will not change. disabling ISR will only not generate the interrupt but HW can change at any time.



createEvent
GetEvent
SetEvent
ClearEvent
WaitEvent -> Blocking
There are two ways to make such a call, which differ in their response to this unavailability: a blocking call results in the task being suspended (put to sleep) and the task will be woken when the request can be fulfilled; a non-blocking call results in an error code being returned and the task has the option of trying the call again later.
