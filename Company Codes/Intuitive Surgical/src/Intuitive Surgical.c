/*
 ============================================================================
 Name        : Intuitive.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>



uint32_t question1(uint32_t a, uint16_t* b)
{
	return ( (a + *(b + 1)) | (uint32_t)*b) ^ (uint32_t)*b;
    //( (0x10101010 + 0x00001010 ) | 0x00001010) ^ 0x00001010;
}


//&i
//0xFFFF0010   10 <- b
//0xFFFF0011   10
//
//0xFFFF0012   10 <- b+1
//0xFFFF0013   10




int main(void) {

	const uint32_t i = 0x10101010;
	const uint32_t answer1 = question1(i, (uint16_t*)&i);
	printf("%d", answer1);
}
