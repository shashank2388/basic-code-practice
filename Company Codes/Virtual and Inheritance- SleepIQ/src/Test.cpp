#include <iostream>

using namespace std;

class Polygon {
	protected:
		int width;
		int height;

	public:
		Polygon(int a, int b)
		{
		    width = a;
		    height = b;
		}

		virtual int area() = 0;

	~Polygon() {}
};

class Output {
	public:
		void print(int i) {
			cout << "Output class STDOUT: " << i << endl;
		}
};


class Rectangle: public Output, public Polygon
{
    public:
        Rectangle(int width, int height):Polygon (width, height) {}

        int area()
        {
            return height * width;
        }

        ~Rectangle() {}
};


class Triangle: public Output, public Polygon
{
    public:
        Triangle(int width, int height):Polygon (width, height) {}

        int area()
        {
            return (width *height) / 2;
        }
};

/* Write your implementations of the Rectangle and Triangle classes here. Both classes must:
	- Have constructors with two integer parameters, height and width.
	- Implement an 'area' method that returns an integer.
	- Inherit from Polygon and Output.
*/


int main(int argc, char *argv[]) {
	int width;
	int height;
	scanf("%d %d", &width, &height);

	Rectangle rectangle(width, height);
	Triangle triangle(width, height);

	rectangle.print(rectangle.area());
	triangle.print(triangle.area());

	return 0;
}
