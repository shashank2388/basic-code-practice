/*
 ============================================================================
 Name        : String.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int calculate(char *s)
{
   int md=-1; // 0 is m, 1 is d
   int sign=1; // 1 is +, -1 is -
   int prev=0;
   int result=0;
   int len = strlen(s);

   for(int i=0; i<len; i++){
       if(isdigit(s[i])){
           int num = s[i]-'0';
           while(++i<len && isdigit(s[i])){
               num = num*10+s[i]-'0';
           }
           i--; // back to last digit of number

           if(md==0){
               prev = prev * num;
               md=-1;
           }else if(md==1){
               prev = prev / num;
               md=-1;
           }else{
               prev = num;
           }
       }else if(s[i]=='/'){
           md=1;
       }else if(s[i]=='*'){
           md=0;
       }else if(s[i]=='+'){
           result = result + sign*prev;
           sign=1;
       }else if(s[i]=='-'){
           result = result + sign*prev;
           sign=-1;
       }
   }

   result = result + sign*prev;
   return result;
   }


int main(void) {

	char *s = "3+2*2";
	printf("%d \n", calculate(s));
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
	return EXIT_SUCCESS;
}
