/*
 ============================================================================
 Name        : ProductArr.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */


//Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].
//
//Example:
//
//Input:  [1,2,3,4]
//Output: [24,12,8,6]
//Note: Please solve it without division and in O(n).

#include <stdio.h>
#include <stdlib.h>

void productArray(int arr[], int n)
{

    // Base case
    if (n == 1) {
//        printf("%d", 0);
        return;
    }

    int i, temp = 1;

    /* Allocate memory for the product array */
    int* prod = (int*) malloc((sizeof(int) * n));

    /* Initialize the product array as 1 */
    memset(prod, 1, n);

    /* In this loop, temp variable contains product of
       elements on left side excluding arr[i] */
    for (i = 0; i < n; i++) {
        prod[i] = temp;
        temp *= arr[i];
    }

    /* Initialize temp to 1
    for product on right side */
    temp = 1;

    /* In this loop, temp variable contains product of
       elements on right side excluding arr[i] */
    for (i = n - 1; i >= 0; i--) {
        prod[i] *= temp;
        temp *= arr[i];
    }

    /* print the constructed prod array */
    for (i = 0; i < n; i++)
        printf("%d \n",prod[i]);

    return;
}

int main(void) {

	int arr[] = { 10, 3, 5, 6, 2 };
	int n = sizeof(arr) / sizeof(arr[0]);
	printf("The product array is: \n");
	productArray(arr, n);
	getchar();
}
