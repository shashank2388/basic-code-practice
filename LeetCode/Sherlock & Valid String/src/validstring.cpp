//============================================================================
// Name        : validstring.cpp
// Author      : Shashank
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>
#include <iterator>
using namespace std;

string isValid(string s) {

    map<char,int> m;
    int l = s.length();
    for(int i = 0; i  < l; i++)
    {
        m[s[i]]++;
    }
    int last = m[s[0]];
    int cnt = 0;
    for(map<char,int> :: iterator itr = m.begin(); itr != m.end(); itr++)
    {
    	cout << (*itr).second;
        if((*itr).second != last)
        {
            cnt++;
        }
    }
    if(cnt <= 1)
    {
        return ("YES");
    }
    else return("NO");

}

int main()
{
//    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string result = isValid(s);
    cout << result;

//    fout << result << "\n";
//
//    fout.close();

    return 0;
}
