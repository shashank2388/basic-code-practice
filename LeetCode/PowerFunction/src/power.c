/*
 ============================================================================
 Name        : power.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float power(float x, int y)
{
	if (y == 0)
		return 1;
	float temp;
	temp = power(x, y/2);

	if ( y% 2 == 0)
		return temp * temp;
	else
	{
		if (y > 0)
			return x * temp * temp;
		else
			return temp * temp / x;
	}
}

int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
	float a = 4;
	int b = 3;
	float c = power(a,b);
	printf("%f", c);
	return EXIT_SUCCESS;
}
