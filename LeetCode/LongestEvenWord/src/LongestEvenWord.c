/**********************************************************************************************************************************************
 ============================================================================
 Name        : LongestEvenWord.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================

Consider a string, sentence, of words separated by spaces where each word is a substring consisting of English alphabetic letters only.
Find the first word in the sentence that has a length which is both an even number and greater than or equal to the length
of any other word of even length in the sentence. If there are multiple words meeting the criteria,
return the one which occurs first in the sentence.

Example

sentence = "Time to write great code"

The lengths of the words are 4, 2, 5, 5, 4, in order. The longest even length words are Time and code.

The one that occurs first is Time, the answer to return



Function Description

Complete the function longestEvenWord in the editor below.



longestEvenWord has the following parameter(s):

    string sentence:  a sentence string



Returns:

    string: the word that is first occurrence of a string with maximal even number length, or the string '0' (zero) if there are no even length words



Constraints

1 ≤ length of sentence ≤ 105
The sentence string consists of spaces and letters in the range ascii[a-z, A-Z, ] only.

**************************************************************************************************************************************************/



#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();

/*
 * Complete the 'longestEvenWord' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING sentence as parameter.
 */

/*
 * To return the string from the function, you should either do static allocation or dynamic allocation
 *
 * For example,
 * char* return_string_using_static_allocation() {
 *     static char s[] = "static allocation of string";
 *
 *     return s;
 * }
 *
 * char* return_string_using_dynamic_allocation() {
 *     char* s = malloc(100 * sizeof(char));
 *
 *     s = "dynamic allocation of string";
 *
 *     return s;
 * }
 *
 */

char* longestEvenWord(char* sentence)
{
	int count = 0;
	int max_count = 0;

	static char s[20];

	char *str = sentence;

	char *start, *curr;
	curr = str;

	while(*str)
	{
		count = 0;
		while (isspace(*str))
		{
			str++;
		}

		curr = str;

		while (*str && !isspace(*str))
		{
			str++;
			curr++;
			count++;
		}

		if (count % 2==0 && count>max_count)
		{
			int i;
			max_count = count;
			start = curr - max_count;
			for (i=0; i<max_count; i++)
			{
				s[i] = start[i];
			}
			s[i] = '\0';
		}
	}

	return s;
}


//char* longestEvenWord(char* sentence)
//{
//	int maxLen = 0;
//
////	static char s[20];
//
//	char *words;
//	const char s[4] = " ";
//
////	words = (char*) malloc(100 * sizeof(char));
//
//	words = strtok(sentence, s);
//
//	while(words != NULL)
//	{
//		if (strlen(words) %2 == 0 && strlen(words) > maxLen)
//		{
//			maxLen = strlen(words);
////			strcpy(s, words);
//		}
//		words = strtok(NULL, " ");
//	}
////	return s;
//}




int main()
{
//    FILE* fptr = fopen(getenv("OUTPUT_PATH"), "w");
	char* sentence = "It is a pleasant day";

//    char* sentence = readline();

    char* result = longestEvenWord(sentence);

    while (*result)
    {
    	printf("%c", *result);
    	result++;
    }

//    fprintf(fptr, "%s\n", result);
//
//    fclose(fptr);

    return 0;
}

char* readline() {
    size_t alloc_length = 1024;
    size_t data_length = 0;

    char* data = malloc(alloc_length);

    while (true) {
        char* cursor = data + data_length;
        char* line = fgets(cursor, alloc_length - data_length, stdin);

        if (!line) {
            break;
        }

        data_length += strlen(cursor);

        if (data_length < alloc_length - 1 || data[data_length - 1] == '\n') {
            break;
        }

        alloc_length <<= 1;

        data = realloc(data, alloc_length);

        if (!data) {
            data = '\0';

            break;
        }
    }

    if (data[data_length - 1] == '\n') {
        data[data_length - 1] = '\0';

        data = realloc(data, data_length);

        if (!data) {
            data = '\0';
        }
    } else {
        data = realloc(data, data_length + 1);

        if (!data) {
            data = '\0';
        } else {
            data[data_length] = '\0';
        }
    }

    return data;
}

