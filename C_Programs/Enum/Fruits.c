/*
*
*Created by:stupkar
*Aug 23, 2017
*8:20:04 AM
*/

#include <stdio.h>

typedef enum
{
	mango,
	banana,
	apple,
	watermelon,
	kiwi
}fruit_type;


int main()
{
	int count[100];

	for ( int i=0; i<100; i++)
	{
		if (fruit_type == mango)
		{
			count [mango]++;
		}

		if (fruit_type == banana)
		{
			count [banana]++;
		}
	}

	printf("No. of mangoes = %d", count[mango]);
	printf("Hello World! \n");
	//Place code here
	return 0;
}
