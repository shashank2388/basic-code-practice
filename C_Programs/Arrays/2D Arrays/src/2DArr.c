/*
 ============================================================================
 Name        : 2DArr.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);

	int r, c;
	printf("Enter the no. of rows and cols \n");
	scanf("%d, %d", &r, &c);

	int **twod_arr = (int**) malloc (sizeof (*twod_arr) * r);

	for(int i=0; i < r; i++) {
		twod_arr[i] = (int**) malloc(sizeof *(twod_arr[i]) *c);
	}

	printf("Enter the elements of the array \n");

	for (int i=0; i<r; i++)
	{
		for (int j=0; j<c; j++)
		{
			scanf("%d", &twod_arr[i][j]);
		}
	}

	for (int i=0; i<r; i++)
	{
		for (int j=0; j<c; j++)
		{
			printf("%d", twod_arr[i][j]);
		}
	}
	return 0;
}

