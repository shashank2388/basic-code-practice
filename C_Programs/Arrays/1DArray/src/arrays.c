/*
 ============================================================================
 Name        : arrays.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


// Code to remove an element from array and return that element
int * remove_no_from_array(int *arr, int val, int no_of_elements)
{
	int x, count;

	for (x = 0; x < no_of_elements; x++ )
	{
		if ( arr[x] != val )
		{
			arr[count++] = arr[x];
		}
	}
	return arr;
}



void productArray(int arr[], int n)
{

    // Base case
    if (n == 1) {
//        printf("%d", 0);
        return;
    }

    int i, temp = 1;

    /* Allocate memory for the product array */
    int* prod = (int*) malloc((sizeof(int) * n));

    /* Initialize the product array as 1 */
    memset(prod, 1, n);

    /* In this loop, temp variable contains product of
       elements on left side excluding arr[i] */
    for (i = 0; i < n; i++) {
        prod[i] = temp;
        temp *= arr[i];
    }

    /* Initialize temp to 1
    for product on right side */
    temp = 1;

    /* In this loop, temp variable contains product of
       elements on right side excluding arr[i] */
    for (i = n - 1; i >= 0; i--) {
        prod[i] *= temp;
        temp *= arr[i];
    }

    /* print the constructed prod array */
    for (i = 0; i < n; i++)
        printf("%d \n",prod[i]);

    return;
}


int main(void) {

	int array[10] = {10,11,12,13,14,15,16,17,18,19};
	int *p;

	p = array; //points to the first location of the array
	printf("%d \n", *p); 	//prints value at first location i.e 10
	printf("%d \n", p); 	//prints address of the first location i.e 6421984
	p++;					//moves the pointer to the second location
	printf("%d \n", p); 	//prints address of the second location i.e 6421988
	*p += 5;				//adds 5 to the value pointed by p i.e second location
	printf("%d \n", *p);	//prints 16 => 11+5
	p = &(p[0]) + 1; 		//increments the address by 1
	*p += 9;				//adds 9 to 12
	printf("%d \n", *p);	//prints 21 => 12+9
	p += 5;					//moves the pointer by 5
	printf("%d \n", p[0]);	//prints 17=> p was pointing to 12 earlier and +5 is 17




	int arr[] = { 1, 2, 5, 6, 2 };
	int n = sizeof(arr) / sizeof(arr[0]);
	printf("The product array is: \n");
	productArray(arr, n);

	return EXIT_SUCCESS;
}
