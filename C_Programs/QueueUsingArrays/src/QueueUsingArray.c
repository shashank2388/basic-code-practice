/*
 ============================================================================
 Name        : QueueUsingArray.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 255

struct QueueNode
{
	int rear, front;
	int a[SIZE];
};

typedef struct QueueNode Queue;

void insert(Queue *q, int val)
{
	if (q->rear == SIZE -1)
	{
		printf("Queue is full");
	}
	else
		q->a[++q->rear] = val;
}

int removeQueue(Queue *q)
{
	if(q->rear == q->front)
	{
		printf("Queue is empty");
		return -1;
	}
	else
	{
		return (q->a[++q->front]);
	}
}

int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */

	Queue *p = NULL;

	insert(p, 10);
	insert(p, 20);
	insert(p, 30);
	int i = removeQueue(p);
	printf("%d", i);

	return EXIT_SUCCESS;
}
