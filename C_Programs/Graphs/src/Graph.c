/*
 ============================================================================
 Name        : Graph.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


struct GNode
{
	int vertex;
	struct GNode *next;
};
typedef struct GNode Graph_Node;

struct Graph
{
	int NumVertices;
	int *visited;
	Graph_Node **adjLists;
};
typedef struct Graph GRAPH;



GRAPH *createGraph (int vertices)
{
	GRAPH *g = (GRAPH*) malloc(sizeof(GRAPH));
	g->NumVertices = vertices;

	g->adjLists = (Graph_Node*) malloc(vertices * sizeof(Graph_Node));

	g->visited = (int*) malloc(vertices *sizeof(int));

	int temp;
	for (temp=0; temp <= vertices; temp++)
	{
		g->adjLists[temp] = NULL;
		g->visited[temp] = 0;
	}
	return g;
}

void addEdge (GRAPH *g, int src, int dest)
{
	// Adding edge from src to dest
	Graph_Node *newnode1 = (Graph_Node*) malloc(sizeof(Graph_Node));
	newnode1->vertex = dest;
	newnode1->next = g->adjLists[src];
	g->adjLists[src] = newnode1;

	// Adding edge from dest to src
	Graph_Node *newnode2 = malloc(sizeof(Graph_Node));
	newnode2->vertex = dest;
	newnode2->next = g->adjLists[dest];
	g->adjLists[dest] = newnode1;
}


void DFS (GRAPH *g, int vertex)
{
	Graph_Node *adjList = g->adjLists[vertex];
	Graph_Node *temp = adjList;

	g->visited[vertex] = 1;
	printf("Visited %d\n", vertex);

	while (temp != NULL)
	{
		int connectedVertex = temp->vertex;

		if(g->visited[connectedVertex] == 0)
		{
			DFS (g, connectedVertex);
		}

		temp = temp->next;
	}
}

void TS (GRAPH *g)
{
	Stack *s;
	for (int i=0; i <g->NumVertices; i++)
	{
		if (g->visited[i] == 0)
		{
			TSUtil(g, i, s);
		}
	}
	while (!isStackEmpty(s))
	{
		printf("%d \n",pop(&s));
	}
}




void printGraph (GRAPH *g)
{
	int v;
	for (v=0; v < g->NumVertices; v++)
	{
		Graph_Node *temp = g->adjLists[v];
		printf("\n Adjacency list of vertex %d\n ", v);
		while (temp)
		{
			printf("%d->", temp->vertex);
			temp = temp->next;
		}
	}

	printf("\n");
}


int main(void) {

	GRAPH *g = createGraph(7);

	addEdge(g, 0, 1);
	addEdge(g, 1, 2);
	addEdge(g, 1, 5);
	addEdge(g, 2, 3);
	addEdge(g, 2, 7);
	addEdge(g, 3, 4);
	addEdge(g, 4, 6);
	addEdge(g, 4, 5);
	addEdge(g, 5, 7);

	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
	printGraph(g);

	DFS(g, 0);

	return EXIT_SUCCESS;
}
