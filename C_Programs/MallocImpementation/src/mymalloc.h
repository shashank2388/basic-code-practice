#include<stdio.h>
#include<stddef.h>

/* To implement your own malloc:
 We consider that a small block is needed at the beginning of each chunk of memory that contains the extra-info about the block called meta-data.
 This meta-data block is stored adjacent to the specific block of allocated memory about which it refers to.
 This would consist of a pointer to next chunk, a flag to mark if the chunk if free and the size of that chunk. 
 This meta-data can be declared as a structure with these 3 data members */


char memory[20000]; 							/* This is the declaration of the array which is considered as our memory.
                       	   	   	   	   	   	   	   We get a contiguous allocation of memory by using an array. */
                       
/*The structure definition to contain metadata of each block allocated or deallocated */
struct block{
     size_t size;               				/* Carries the size of the block described by it */
     int free;                  				/* This flag is used to know whether the block described by the metadata structure is free or not -- > if free, it is set to 1. Otherwise 0. */
     struct block *next;        				/* Points to the next metadata block */
};

struct block *freeList=(void*)memory;       	/* Pointing to the main block of memory which is initially free (no storage allocation yet)
 	 	 	 	 	 	 	 	 	 	 	 	 Here we initialize a pointer of type "block", named freeList pointing to the starting address of the chunk of memory we created before.
 	 	 	 	 	 	 	 	 	 	 	 	 This freeList pointer will point to the start of the linked list of metadata blocks.
 	 	 	 	 	 	 	 	 	 	 	 	 The starting address of the array (memory) should be casted to type void so that we are able to allocate blocks of memory which are of different datatypes.(int, char, float etc.)
 	 	 	 	 	 	 	 	 	 	 	 	 */

void initialize();                              /* to initialize the block of memory*/

void split(struct block *fitting_slot,size_t size); /* to make way for a new block allocation by splitting a free block -- (Assume first fit algorithm)*/

void *MyMalloc(size_t noOfBytes);

void merge();

void MyFree(void* ptr);
