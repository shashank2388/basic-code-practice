/*
 ============================================================================
 Name        : Bitwise.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#define MASK(x) ((unsigned char)(1<<x))


/*Bit manipulations using #define */
#define BIT(n)				(1 << (n))
#define SET_BIT(y, n)		(y |= BIT(n))
#define RESET_BIT(y, n)		(y &= ~BIT(n))
#define TOGGLE_BIT(y, n)	(y ^= BIT(n))

#define NO_OF_BITS 			32

#define R2(n)     	n,     n + 2*64,     n + 1*64,     n + 3*64
#define R4(n) 	R2(n), R2(n + 2*16), R2(n + 1*16), R2(n + 3*16)
#define R6(n) 	R4(n), R4(n + 2*4 ), R4(n + 1*4 ), R4(n + 3*4 )

#define rev_look_up	R6(0), R6(2), R6(1), R6(3)

#define B2(n) 		n,     n+1,     n+1,     n+2
#define B4(n) 	B2(n), B2(n+1), B2(n+1), B2(n+2)
#define B6(n) 	B4(n), B4(n+1), B4(n+1), B4(n+2)

#define set_look_up B6(0), B6(1), B6(1), B6(2)


int reverseBits_using_lookup (unsigned int num)
{

	unsigned int rev_bits_lookuptable[256] = { rev_look_up };
    int reverse_num = 0;
     // Reverse and then rearrange

                   // first chunk of 8 bits from right
     reverse_num = rev_bits_lookuptable[ num & 0xff ]<<24 |
                   	   // second chunk of 8 bits from  right
    		 	 	 rev_bits_lookuptable[ (num >> 8) & 0xff ]<<16 |
					 rev_bits_lookuptable[ (num >> 16 )& 0xff ]<< 8 |
					 rev_bits_lookuptable[ (num >>24 ) & 0xff ] ;

    return reverse_num;
}

unsigned int reverseBits(unsigned int num)
{
    unsigned int no_of_bits = sizeof(num) * 8;
    unsigned int reverse_num = 0;
    int i;
    for (i = 0; i < no_of_bits; i++)
    {
        if((num & (1 << i)))
           reverse_num |= 1 << ((no_of_bits - 1) - i);
    }
    return reverse_num;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


uint16_t count_bit_using_lookup (uint32_t v)
{
	unsigned int cout_bit_lookuptable[256] = { set_look_up };
	uint16_t c; // c is the total bits set in v

	c =	cout_bit_lookuptable[v & 0xff] + cout_bit_lookuptable[(v >> 8) & 0xff] +
			cout_bit_lookuptable[(v >> 16) & 0xff] + cout_bit_lookuptable[v >> 24];

	return c;
}

int countSetBits(int n)
{
	int count = 0;
	while (n)
	{
		count += (n & 0x01);
		n >>= 1;
	}
	return count;
}

//we subtract a number n by 1 and do bitwise & with itself (n & (n-1)), we unset the rightmost set bit
int countsetbits(int n)
{
	int count = 0;
	while (n)
	{
		n &= (n-1);
		count++;
	}
	return count;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int rotateBitsLeft(int val, int n)
{
	return ((val<<n) | (val >>(NO_OF_BITS - n)));
}

int rotatBitsRight(int val, int n)
{
	return ((val>>n) | (val << (NO_OF_BITS - n)));
}

// If we subtract power of 2 number for example n by 1, and then do the bitwise &. if (n &[n-1]) = 0, we can say the no is power of 2
int PowerOfTwo (int x)
{
	return x && (!(x & (x-1)));
}

//void foo (uint32_t bar, uint8_t x, uint8_t y)
//{
//	bar = ((bar & 0xFFFFF0000)| x) | y<<8;
////	bar = (bar & 0xFFFFF00FF)| y<<8;
//
//	printf("%d", bar);
//}


// Function to set bits 2 and 5, clear bits 0,3 & 7. If bit 4 is high invert bit 1.
void manage_Port_B ()
{
	uint8_t temp;

	//temp = PTBD;   //Required if we pass any value to the function

	temp = temp | (MASK(2)|MASK(5));

	temp = temp &~ (MASK(0)|MASK(3)|MASK(7));

	if (temp & MASK(4))
		temp ^= MASK(1);

	//PTBD = temp; //Store the value back in the port register
}


// Code to Invert Bits of a number

int invertBits (int no)
{
	int total_bits = log (no); // stores the total no. of bits in n

	int one_n = 1 << total_bits; // These 2 steps
	one_n = one_n | one_n - 1;	// generates one_n which has all 1s of total_bits

	no = no ^ one_n;

	return no;
}

int main(void) {
	int a = 255;
//	printf("%d \n", count_bit_using_lookup(a));

//	unsigned int lookuptable[256] = { set_look_up };
//	for (int i=0; i <255; i++)
//	{
//		for (int j=0; j<8; j++)
//		{
//			printf("%d \t", lookuptable[i]);
//		}
//
//		printf("\n");
//	}

//	foo (30, 1, 5);
	return EXIT_SUCCESS;
}
