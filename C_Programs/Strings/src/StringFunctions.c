/*
 ============================================================================
 Name        : StringFunctions.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ctype.h"

int string_length(char *s)
{
	int count = 0;
	while ( *s != '\0')
	{
		count++;
		s++;
	}

	return count;
}


int string_compare (char *s, char *p)
{
	while (*s == *p)
	{
		if (*s == '\0' || *p == '\0')
			return 0;
		s++;
		p++;
	}

	return (*s - *p);
}

// Reverse without recurssion
void reverse_string (char *s)
{
	int length, c;


	char *begin, *end, temp;
	begin = s;
	end = s;

	length = strlen(s);


    for (c = 0; c < length - 1; c++)
    {
    	end++;
    }

	for (c=0; c<length/2; c++)
	{
		temp = *end;
		*end = *begin;
		*begin = temp;

		begin++;
		end--;
	}
}

// Reverse a string using recursion
void reverseString(char *string, int begin, int end)
{
	char temp;
	if (begin >= end)
		return;

	temp = *(string+begin);
	*(string+begin) = *(string+end);
	*(string+end) = temp;

	reverseString(string, ++begin, --end);
}


// Code to remove white spaces in the string
void removewhitespace(char *str)
{
	int count = 0;

	for(int i = 0; str[i]; i++)
	{
		if (str[i] != ' ')
			str[count++] = str[i];
	}
	str[count] = '\0';
}



int findFirstDuplicateInteger(char *str)
{
    int len  = strlen(str);
    int no[20];
	int i, j;

    while ( *str != '\0' )
    {
        if ( isdigit (*str) )
        {
            no[i] = (int) (*str - '0');
            i++;
        }
        str++;
    }
    for ( i = 0; i < len; i++)
    {
        for (j = i+1; j < len; j++)
        {
            if ( no[i] == no [j])
                return no [i];
        }
    }
    return -1;
}

int main(void) {

	char s[100], s1[100];

	gets(s);


	printf("%d", string_length(s));

	reverse_string(s);
	printf("%s \n", s);

	printf("Enter new string");
	gets(s1);

	int result = string_compare(s, s1);
	printf("%d", result);

	return EXIT_SUCCESS;
}
