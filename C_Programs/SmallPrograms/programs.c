/*
*
*Created by:stupkar
*Aug 6, 2017
*5:44:25 PM
*/

#include <stdio.h>
#include <string.h>
#include "ctype.h"


// Code to find the inverse of a number
float inverseValue (int value)
{
	 float return1;
	 return1 = (1/(float) value);
	 return return1;
}

// Return Code to find the first n fibonacci number
int Fibonacci(int n)
{
   if ( n <= 1 )
      return n;
   else
      return ( Fibonacci(n-2) + Fibonacci(n-1) );
}

// Recurssive function to check if the no if Prime or not
int isPrime(int num, int i)
{
	if (i==1)
		return 1;
	else if(num % i == 0)
		return 0;
	else
		return isPrime(num, i-1);
}

// Iterative code to check if the no if Prime or not
int IsPrime(int number) {
    int i;
    for (i=2; i<number; i++) {
        if (number % i == 0 && i != number) return 0;
    }
    return 1;
}

// Power function power(x,y) should return x^y. covers negative numbers too
double power(float x, int y)
{
	double temp;

	if (y==0)
		return 1;

	temp = power(x, y/2);

	if (y % 2 == 0)
		return temp * temp;

	else
	{
		if (y>0)
			return x * temp * temp;
		else
			return (temp * temp) / x;
	}
}

// Code to check if stack grows up or down
void check_fun (int *main_local_addr)
{
	int *local_var;
	
	if (main_local_addr < &local_var)
		printf("Stack grows upwards");
	else
		printf("Stack grows downards");
}

int main()
{

	int x = Fibonacci(6);
	printf("%d \n", x);

	if (x % 3 == 0)
	{
		if (x == 3 || !IsPrime(x))
			printf ("Buzz \n");
	}
	if (x % 5 == 0)
	{
		if (x == 5 || !IsPrime(x))
			printf ("Fizz \n");
	}
	if (IsPrime(x) && x != 1 && x != 0)
	{
		printf ("BuzzFizz \n");
	}
	else
		printf("%d \n", x);

	int main_var;

	check_fun(*main_var);

	return 0;
}
