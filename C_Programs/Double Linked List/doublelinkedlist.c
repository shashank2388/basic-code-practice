/*
*
*Created by:stupkar
*Aug 5, 2017
*3:47:04 PM
*/

#include <stdio.h>
#include <stdlib.h>

struct doubleNodeType
{
	int data;
	struct doubleNodeType *next;
	struct doubleNodeType *prev;
};

typedef struct doubleNodeType doublelinkNODE;

void insert_doubleLinkedList_start (doublelinkNODE **p, int value)
{
	doublelinkNODE *newnode, *temp;

	temp = *p;

	newnode = (doublelinkNODE *) malloc(sizeof(doublelinkNODE));
	newnode->data = value;

	newnode->next = *p;
	newnode->prev = NULL;

	if (*p != NULL)
		temp->prev = newnode;

	*p = newnode;
}

void insert_doubleLinkedList_last (doublelinkNODE **p, int value)
{
	doublelinkNODE *newnode, *temp;

	newnode = (doublelinkNODE *) malloc(sizeof(doublelinkNODE));
	newnode->data = value;

	newnode->next = NULL;

	if (*p == NULL)
	{
		newnode->prev = NULL;
		*p = newnode;
		return;
	}
	else
	{
		temp = *p;
		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = newnode;
		newnode->prev = temp;
	}
}

void printdoubleLinkedList(doublelinkNODE *p)
{
	doublelinkNODE *temp;

	temp = p;

	while (temp != NULL)
	{
		printf ("%d \n", temp->data);
		temp= temp->next;
	}
}

void deleteNode (doublelinkNODE **p, doublelinkNODE *del)
{
	// Case 1: If the double linked list is empty or the node to be deleted is empty
	if (*p == NULL || del == NULL)
	{
		return;
	}

	//Case 2: If the node to be deleted is pointed by head
	if (*p == del)
		*p=del->next;

	// Case 3: If the node to be deleted is in the middle of the list
	if (del->next != NULL)
		del->next->prev = del->prev;
	if (del->prev != NULL)
		del->prev->next = del->next;

	free (del);
}

void reverseDoubleLL (doublelinkNODE **p)
{
	doublelinkNODE *temp;
	doublelinkNODE *current;

	current = *p;
	while (current != NULL)
	{
		temp=current->prev;
		current->prev = current->next;
		current->next = temp;
		current = current->prev;
	}
	if (temp!=NULL)
		*p = temp->prev;
}


int main()
{
	doublelinkNODE *head;
	insert_doubleLinkedList_last(&head, 10);
	insert_doubleLinkedList_last(&head, 20);
	insert_doubleLinkedList_last(&head, 30);
	insert_doubleLinkedList_last(&head, 40);
	printdoubleLinkedList(head);
	insert_doubleLinkedList_start(&head, 50);
	printdoubleLinkedList(head);

	printf("Hello World! \n");

	deleteNode(&head, head->next);
	printdoubleLinkedList(head);
	return 0;
}
