/*
 * LinkedList.c
 *
 *  Created on: Aug 1, 2019
 *      Author: uidm5416
 */

#include <stdio.h>
#include <stdlib.h>


// Declare bool in C
typedef enum
{
	FALSE,
	TRUE
} bool;

// NODE structure
struct NodeType
{
	int data;
	struct NodeType *link;
};
typedef struct NodeType NODE;

// Code to insert Node in the Linked List
void insert(NODE **p, int value)
{
	NODE *temp;
	NODE *newnode;

// Assigning a new node
	newnode = (NODE*) malloc(sizeof(NODE));
	newnode->data = value;
	newnode->link = NULL;

	if (*p == NULL)
		*p = newnode;
	else
	{
		temp = *p;
		while(temp->link != NULL)
		{
			temp = temp->link;
		}
		temp->link = newnode;
	}
}

// Code to insert after particular value in Linked list
void insertafter(NODE **p, int value, int dat)
{
	NODE *newnode, *temp;

	newnode = (NODE*) malloc(sizeof(NODE));
	newnode->data = value;
	newnode->link = NULL;

	temp = *p;

	while (temp->data != dat)
	{
		temp = temp->link;
	}
	newnode->link = temp->link;
	temp->link=newnode;
}

// Code to print Linked List
void printLinkedList(NODE *p)
{
	NODE *temp;

	temp = p;

	while (temp != NULL)
	{
		printf ("%d \n", temp->data);
		temp= temp->link;
	}
}

// Code to reverse a Linked List
void reverseLinkedList(NODE **p)
{
	NODE *curr_node = NULL;
	NODE *prev_node = NULL;
	NODE *next_node = NULL;

	curr_node = *p;

	while (curr_node != NULL)
	{
		next_node = curr_node->link;
		curr_node->link = prev_node;
		prev_node = curr_node;
		curr_node = next_node;
	}
		*p = prev_node;
}

// Code to reverse a Linked List recusively
void rec_reverseLinkedList(NODE **p)
{
	NODE *first;
	NODE *rest;

	if (*p == NULL )
		return;

	first = *p;
	rest = first->link;

	if (rest == NULL)
		return;

	rec_reverseLinkedList (&rest);

	first->link->link = first;
	first -> link = NULL;

	*p = rest;
}

// Code to delete a NODE from Linked List
void deleteNode (NODE **p, int value)
{
	NODE *previous;
	NODE *current;
	bool found = FALSE;

	current = *p;
// Case 1: If linked list is empty
	if (p == NULL)
		return;

// Case 2: If the head is pointing to the value to be deleted
	if (current->data == value)
	{
		*p = current->link;
		free (current);
		return;
	}
	else
	{
// Case 3: If the value to be deleted is in the middle of the list
//		   first find the data to be deleted
		previous = *p;
		while (current != NULL && !found)
		{
			if(current->data != value)
			{
				previous = current;
				current = current->link;
			}
			else
				found = TRUE;
		}

		if (found)
		{
			previous->link = current->link;
			free (current);
		}
	}

	printf("Data not found \n");
}

// Code to detect loop in the linked list
int detectloop (NODE *p)
{
	NODE *slow_ptr;
	NODE *fast_ptr;

	slow_ptr = p;
	fast_ptr = p->link;

	while (slow_ptr != NULL && fast_ptr != NULL)
	{
		if(slow_ptr == fast_ptr)
			return 1;

		slow_ptr = slow_ptr->link;
		fast_ptr = fast_ptr->link->link;
	}
	return 0;
}

// Code to find middle of the linked list
int findMiddle (NODE *p)
{
	NODE *slow_ptr;
	NODE *fast_ptr;

	slow_ptr = p;
	fast_ptr = p->link;

	while (fast_ptr  && fast_ptr->link)
	{
		slow_ptr = slow_ptr->link;
		fast_ptr = fast_ptr->link->link;
	}
	return slow_ptr->data;
}


// Checks if the value is present in the list: If yes returns 1 else returns 0
int isPresent(NODE *r, int val)
{
	NODE *temp;

	temp = r;

	while (temp != NULL)
	{
		if(temp->data == val)
		{
			return 1;
		}

		temp = temp->link;
	}

	return 0;
}

// To get union of 2 linked lists (merge 2 lists leaving the common values)
NODE *getUnion (NODE *p, NODE *q)
{
	NODE *result = NULL;
	NODE *temp1, *temp2;

	temp1 = p;
	temp2 = q;

	while (temp1 != NULL)
	{
		insert (&result, temp1->data);
	}

	while (temp2 != NULL)
	{
		if (!isPresent(result, temp2->data))
		{
			insert(&result, temp2->data);
		}
		temp2 = temp2->link;
	}

	return result;
}


// Code to find the number of nodes in the linked list
int getCount (NODE *p)
{
	NODE *temp;
	int count = 0;
	temp = p;

	if(p == NULL)
		return 0;

	while (temp != NULL)
	{
		count++;
		temp = temp->link;
	}
	return count;
}

// Code to get the intersection node
int getIntersectingNode (int diff, NODE *p, NODE *q)
{
	NODE *curr1 = p;
	NODE *curr2 = q;
	int i;

	for (i=0; i<diff; i++)
	{
		if (curr1 == NULL)
			return -1;
		else
			curr1 = curr1->link;
	}
	while (curr1 != NULL && curr2 != NULL)
	{
		if (curr1 == curr2)
			return curr1->data;
		curr1 = curr1->link;
		curr2 = curr2->link;
	}
	return -1;
}

// Code to find whether the linked list intersect each other and return the intersecting point
void getIntersectionNode (NODE *head1, NODE *head2)
{
	int c1 = 0, c2 = 0, diff = 0;

	c1 = getCount(head1);
	c2 = getCount(head2);

	if (c1 > c2)
	{
		diff = c1 - c2;
		getIntersectingNode(diff, head1, head2);
	}
	else
	{
		diff = c2 - c1;
		getIntersectingNode(diff, head2, head1);
	}
}

// Merge two sorted linked list recursively
NODE *SortedMerge(NODE *a, NODE *b)
{
  NODE *result = NULL;

  /* Base cases */
  if (a == NULL)
     return(b);
  else if (b==NULL)
     return(a);

  /* Pick either a or b, and recur */
  if (a->data <= b->data)
  {
     result = a;
     result->link = SortedMerge(a->link, b);
  }
  else
  {
     result = b;
     result->link = SortedMerge(a, b->link);
  }
  return(result);
}

/* Delete Nth Node from last 
    Take two pointers – fast and slow. And initialize their values as head node
    Iterate fast pointer till the value of n.
    Now, start iteration of fast pointer till the None value of the linked list. Also, iterate slow pointer.
    Hence, once the fast pointer will reach to the end the slow pointer will reach the node which you want to delete.
    Replace the next node of the slow pointer with the next to next node of the slow pointer.
*/
NODE *deleteNnode(NODE *head, int key)
{
	if(head==NULL) {
		cout<<"Err : List is empty"<<endl;
		return head;
	}
	
	if(key<=0) {
		cout<<"Err : Number should be positive"<<endl;
		return head;
	}
        
	NODE *fast = head;
	NODE *slow = head;

	for (int i=0; i<key; i++)
	{
		if(fast == NULL && i != n-1) 
		{
            cout<<"Err : Number is greater than length of List"<<endl;
            return head;
        }
        fast = fast->next;
    }
	if (fast->link ==NULL)
		return head->link;

	while(fast->link != NULL)
	{
		fast = fast->link;
		slow = slow->link;
	}
	slow->link = slow->link->link;
	return head;
} 

int main ()
{
	NODE *head;
	insert(&head, 10);
	insert(&head, 20);
	insert(&head, 30);
	insert(&head, 40);
	insert(&head, 50);
	insert(&head, 60);
	printf("Hello World");
	insertafter(&head, 55, 50);
	printLinkedList(head);
	printf("After reverse \n");
//	RecReverse(&head);
//	PrintLinkedList(head);
//	NODE *result = OddEvenNode(head);
//	PrintLinkedList(result);

	return 0;
}
