/*
 * BT.c
 *
 *  Created on: Aug 27, 2015
 *      Author: Shashank
 */

#include <stdio.h>
#include <stdlib.h>

struct BTNode
{
	int value;
	struct BTNode *leftlink;
	struct BTNode *rightlink;
};

typedef struct BTNode BTree;

void insertBT (BTree**p, int data)
{
	BTree *newnode = NULL;
	if (*p == NULL)
	{
		newnode = (BTree*)malloc(sizeof(BTree));
		newnode -> value = data;
		newnode -> leftlink = NULL;
		newnode -> rightlink = NULL;
		*p = newnode;
	}
	else
	{
		if ( data < (*p)->value )
		{
			insertBT(&(*p)->leftlink, data);
		}
		else if (data > ((*p)->value))
		{
			insertBT(&(*p)->rightlink, data);
		}
	}
}


void displayTree(BTree *p)
{
	if(p ==NULL)
		return;
	else
	{
		displayTree(p->leftlink);
		printf("%d\n", p->value);
		displayTree(p->rightlink);
	}
}

int main()
{
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	printf("Binary Tree\n");
	BTree *first = NULL;
	insertBT(&first, 10);
	insertBT(&first, 20);
	insertBT(&first, 30);
	insertBT(&first, 40);
	insertBT(&first, 50);
	displayTree(first);

}
