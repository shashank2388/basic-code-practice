/*
 ============================================================================
 Name        : QueueUsingLL.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

struct NodeType
{
	int data;
	struct Node *next;
};

typedef struct NodeType Node;

struct QueueNode
{
	Node *front, *rear;
};

typedef struct QueueNode Queue;

void insertq (Queue *p, int val)
{
	Node *newnode;

	newnode = (Node*) malloc(sizeof(Node));
	newnode->data = val;
	newnode->next = NULL;

	if (p->rear == NULL)
	{
		p->front = newnode;
		p->rear = newnode;
	}

	else
	{
		p->rear->next = newnode;
		p->rear = newnode;
	}
}


int removeq (Queue *p)
{
	if (p->front == NULL)
	{
		printf("Queue Empty");
		return 0;
	}

	Node *temp;
	temp = p->front;
	int val = p->front->data;

	p->front = p->front->next;

	if (p->front == NULL)
		p->rear = NULL;

	free(temp);

	return val;

}

int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */

	Queue* q =(Queue*)malloc (sizeof(Queue)) ;
	q->front = NULL;
	q->rear = NULL;

	insertq (q, 10);
	insertq (q, 20);
	int i = removeq(q);

	printf("%d", i);

	return EXIT_SUCCESS;
}
