/*
 ============================================================================
 Name        : StacksUsingLL.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

struct StackNodeType
{
	int value;
	struct StackNodeType *next;
};

typedef struct StackNodeType Stack;


void push(Stack **S, int data)
{
	Stack *newnode;
	newnode = (Stack*) malloc(sizeof(Stack));
	newnode->value = data;
	newnode->next = *S;
	*S = newnode;
}

int pop (Stack **S)
{
	if (*S == NULL)
	{
		printf("Stack empty");
	}

	Stack *temp;
	int value;

	temp = *S;
	*S = temp->next;
	value = temp->value;

	free(temp);
	return value;
}

int isStackEmpty(Stack **S)
{
	return (*S == NULL);
}


int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
	return EXIT_SUCCESS;
}
