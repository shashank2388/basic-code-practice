/*
 ============================================================================
 Name        : Conversion.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// String to Integer Conversion
int myatoi (char *str)
{
	int value = 0;
	int sign = 1;

	if (*str == '\0')
		return 0;

	if (*str == '+' || *str == '-' || *str == '\n' || *str == ' ' || *str == '\t')
	{
		if (*str == '-')
			sign = -1;

		str++;
	}

	while(*str != '\0')
	{
		if (*str >= '0' && *str <= '9')
		{
			value *= 10;
			value += (int)(*str - '0');
			str++;
		}
		else
			return 0;
	}

	return (sign * value);
}


// Helper function for ftoa
void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}


// Helper function for ftoa
int inttoStr(int integer, char str[], int dec)
{
	int i = 0;

	while(integer)
	{
		str[i++] = (integer % 10) + '0';
		integer = integer/10;
	}

	while (i < dec)
	{
		str[i++] = '0';
	}

	reverse(str, i);
	str[i] = '\0';
	return i;
}

// Floating point to String Conversion
void ftoa (float no, char *res, int afterpoint)
{
	int ipart = (int)no;

	float fpart = no - (float)ipart;

	int i = inttoStr(ipart, res, 0);

	if (afterpoint != 0)
	{
		res[i] = '.';

		fpart = fpart * pow(10, afterpoint);

		inttoStr((int)fpart, res+i+1, afterpoint);
	}
}


// Integer to String
//							   	 O/P
// myitoa (1567, str, 10) 	-> 	"1567"
// myitoa (-1567, str, 10)	->	"-1567"
// myitoa (1567, str, 2)	->	"11000011111"

char *itoa (int num, char *str, int base)
{
	int i=0;
	int isNegative = 0;

	if (num == 0)
	{
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}

	if (num < 0 && base == 10)
	{
		isNegative = 1;
		num = -num;
	}

	while (num!= 0)
	{
		int rem = num % base;
		str[i++] = (rem>9)? (rem -10)+'a': rem+ '0';
		num = num /base;
	}

	if (isNegative)
	{
		str[i++] = '-';
	}
	str[i] = '\0';

	reverse (str,i);

	return str;
}


int main(void) {
//	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
//	return EXIT_SUCCESS;

	unsigned int a = 6;
	int b = -20;

	printf("%d", a+b);

	(a+b > 6) ? puts(">6") : puts("<=6");
}
