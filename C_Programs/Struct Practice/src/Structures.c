/*
 ============================================================================
 Name        : Structures.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>



struct ABC
{
	uint32_t a:1;
};

struct ABC2
{
	char a:4;
	int c:4;
	char b:4;

};

typedef struct ABC abc;
typedef struct ABC2 abc2;

int main(void) {

	printf("Size of struct ABC is %lu \n", sizeof(abc));
	printf("Size of struct ABC2 is %lu \n", sizeof(abc2));

	char a = '1';

	printf("%d",a-'0');

	return EXIT_SUCCESS;
}



