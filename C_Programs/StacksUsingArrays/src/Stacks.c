/*
 ============================================================================
 Name        : Stacks.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 255

struct StackNode
{
	int top;
	int a[SIZE];
};

typedef struct StackNode Stack;

void push (Stack *S, int val)
{
	if (S->top == SIZE - 1)
	{
		printf ("Stack is full");
		return;
	}

	else
	{
		S->a[++S->top] = val;
	}
}

int pop (Stack *S)
{
	if (S->top == -1)
	{
		printf("Stack is empty");
		return -1;
	}

	else
	{
		return S->a[S->top--];
	}
}

int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
	return EXIT_SUCCESS;
}
