/*
 ============================================================================
 Name        : memory.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

// simple implementation
void mymemcpy1(void *dest, const void *source, size_t num) {
    int i = 0;
    // casting pointers
    char *dest8 = (char *)dest;
    char *source8 = (char *)source;
    printf("Copying memory %lu byte(s) at a time\n", sizeof(char));
    for (i = 0; i < num; i++) {
        dest8[i] = source8[i];
    }
}

// it checks that destination array does not overwrite
// source memory
void mymemcpy2(void *dest, const void *source, size_t num) {
    int i = 0;
    // casting pointers
    char *dest8 = (char *)dest;
    char *source8 = (char *)source;

    printf("Copying memory %d byte(s) at a time\n", sizeof(char));

    for (i = 0; i < num; i++) {
        // make sure destination doesnt overwrite source
        if (dest8[i] == source8) {
            printf("destination array address overwrites source address\n");
            return;
        }
        dest8[i] = source8[i];
    }
}

// copies 1 word at a time (8 bytes at a time)
void mymemcpy3(void *dest, const void *source, size_t num) {
    int i = 0;
    // casting pointers
    long *dest32 = (long *)dest;
    long *source32 = (long *)source;
    char *dest8 = (char *)dest;
    char *source8 = (char *)source;

    printf("Copying memory %d bytes at a time\n", sizeof(long));

    for (i = 0; i < num/sizeof(long); i++) {
        if (dest32[i] == source32) {
            printf("destination array address overwrites source address\n");
            return;
        }
        dest32[i] = source32[i];
    }

    // copy the last bytes
    i*=sizeof(long);
    for ( ;i < num; i++) {
        dest8[i] = source8[i];
    }
}

/* memory address is n-byte aligned when address is multiple of n bytes
   b-bit aligned is equivalent to b/8-byte aligned
   padding = n - (offset &amp;amp; ( -1)) = -offset &amp;amp; (n-1)
   aligned offset = (offset + n-1) &amp;amp; ~(n-1)

   Copies 8 bytes at a time with destination align to 64-byte boundary
*/
void mymemcpy4(void * dest, const void * source, size_t size) {

#define NBYTE 8 // n-byte boundary

    int i = 0;
    int j = 0;

    // short and long pointers for copying 1 and 8 (sizeof(long))
    //  bytes at a time
    long * destlong = (long *)dest;
    long * sourcelong = (long *)source;
    char * destshort = (char *)dest;
    char * sourceshort = (char *)dest;


    // copy first bytes until destination hits 64-byte boundary
    while((((uintptr_t)destshort[i] & (uintptr_t)(NBYTE-1)) != 0) && (destshort[i] != source) && (i < size)) {
        destshort[i] = sourceshort[i];
        i++;
    }
    printf("%s: copied %d bytes up to %d-byte alignment\n",
           __func__, i, NBYTE);


    // now copy 8 (sizeof(long)) bytes at a time with dest aligned
    // align destination pointer
    destlong = ((uintptr_t)destlong + (NBYTE-1)) & ~(uintptr_t)(NBYTE-1);

     // continue copying where left off (we should align source as well...)
    sourcelong = (long *)sourceshort;
    // j+1 * 8 - 1 to avoid copying beyond last element in last iteration
    while ((j+1)*sizeof(long) - 1 + i < size &&  destlong[j] < (long *)source) {
        destlong[j] = sourcelong[j];
        j++;
    }
    printf("%s: copied %d bytes %d bytes at a time\n", __func__,
           j*sizeof(long), sizeof(long));


    // finally copy last bytes
    i = i + j*sizeof(long);
    int prev_i = i;
    while(i < size && destshort[i] < (char *)source) {
        destshort[i] = sourceshort[i];
        i++;
    }
    printf("%s: copied last %d bytes one at a time\n",
            __func__, i-prev_i);

}

/* simpler implementation of mymemcpy4 */
void mymemcpy4b(const void * dest, void * src, size_t size) {

    char * dest1 = (char *)dest;
    char * src1 = (char *)src;
    int n = 0;
#define NBYTE 64
    // copy up to nbyte alignment
    while ((((uintptr_t)dest1 & (NBYTE-1)) != 0x00) && (n < size))
    {
        *dest1++ = *src1++;
        n++;
    }
    printf("copied %d bytes at a time up to %d bytes\n", sizeof(char), n);

    // copy up to end minus last sizeof(long) bytes
    long * dest2 = (long *)dest1;
    long * src2 = (long *)src1;
    while (n < size - sizeof(long)) {
        *dest2++ = *src2++;
        n+=sizeof(long);
    }
    printf("copied %d bytes at a time up to %d bytes\n", sizeof(long), n);

    // copy last bytes
    src1 = (char *)src2;
    dest1 = (char *)dest2;
    while (n < size) {
        *dest1++ = *src1++;
        n++;
    }
    printf("copied up to %d bytes\n", n);
}




int main(int argc, char **argv) {
 char source[] = "0123456789abdcdefghi";  // 21 bytes (20 letters + '\0')
    char dest[100];

    // void * memcpy ( void * destination, const void * source, size_t num );
    memcpy(dest, source, strlen(source) + 1);
    printf("Source: %s. Destination: %s\n", source, dest);

    strcpy(source, "0123456789abdcdefghi");
    mymemcpy1(dest, source, strlen(source) + 1);
    printf("Source: %s. Destination: %s\n", source, dest);

    strcpy(source, "0123456789abdcdefghi");
    mymemcpy2(dest, source, strlen(source) + 1);
    printf("Source: %s. Destination: %s\n", source, dest);

    strcpy(source, "0123456789abdcdefghi");
    mymemcpy3(dest, source, strlen(source) + 1);
    printf("Source: %s. Destination: %s\n", source, dest);

    strcpy(source, "0123456789abdcdefghi");
    mymemcpy4(dest, source, strlen(source) + 1);
    printf("Source: %s. Destination: %s\n", source, dest);
    return 0;
}

