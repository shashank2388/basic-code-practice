/*
 ============================================================================
 Name        : Camera.c
 Author      : Shashank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

/*--------------------------------------------------------------------------------------
Capture images using V4L2 on Linux
V4L2 is the second version of Video For Linux which is a video capturing API for Linux.
Step 1: Open the Capture Device.
In Linux, default capture devide is generally /dev/video0
Step 2: Query the Capture
Step 3: Image Format
Step 4: Request Buffers
Step 5: Query Buffer
Step 6: Capture Image
Step 7: Store data
-----------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <linux/videodev2.h>
#include <jpeglib.h>

#define CLEAR(x) memset (&(x), 0, sizeof (x))

#define CLIP(x) ( (x)>=0xFF ? 0xFF : ( (x) <= 0x00 ? 0x00 : (x) ) )

struct buffer {
        void *                  start;
        size_t                  length;
};

/*
struct Pixels
{
   unsigned char Blue,Green,Red;
};

int size_spix = sizeof(struct Pixels*);
struct Pixels **pixel_arrayp;
*/

static int              fd              = -1;
struct buffer *         buffers         = NULL;
static unsigned int     n_buffers       = 0;

// global settings
// Modified for arm-11 board
static unsigned int width = 320;
static unsigned int height = 240;
static unsigned char jpegQuality = 70;
static char* jpegFilename = NULL;
static char* deviceName = "/dev/video0";

// To write a file
FILE *out;


/**
  Convert from YUV422 format to RGB888. Formulae are described on http://en.wikipedia.org/wiki/YUV

  \param width width of image
  \param height height of image
  \param src source
  \param dst destination
*/

static void YUV422toRGB888(int width, int height, unsigned char *src, unsigned char *dst)
{
	int line, column;
	unsigned char *py, *pu, *pv;
	unsigned char *tmp = dst;
	unsigned char r, g, b;

	/* In this format each four bytes is two pixels. Each four bytes is two Y's, a Cb and a Cr.
     Each Y goes to one of the pixels, and the Cb and Cr belong to both pixels. */
	py = src;
	pu = src + 1;
	pv = src + 3;



	/*allocate memory*/
	/*   pixel_arrayp = (struct Pixels **)calloc(height,size_spix);

   for(line=0;line<height; line++) {
      pixel_arrayp[line] = (struct Pixels *)calloc(width,size_spix);
   }
	 */

	for (line = 0; line < height; ++line) {
		for (column = 0; column < width; ++column) {
			*tmp = CLIP((double)*py + 1.402*((double)*pv-128.0));
			r = *tmp++;
			//fwrite(r[line][column], sizeof(r*height*width), 1, out);

			*tmp = CLIP((double)*py - 0.344*((double)*pu-128.0) - 0.714*((double)*pv-128.0));
			g = *tmp++;

			*tmp = CLIP((double)*py + 1.772*((double)*pu-128.0));
			b = *tmp++;

			fprintf(out,"r:%d	g:%d	b:%d \n", r, g, b);

			// increase py every time
			py += 2;
			// increase pu,pv every second time
			if ((column & 1)==1) {
				pu += 4;
				pv += 4;
			}
		}
	}
}

/**
  Print error message and terminate programm with EX-IT_FAILURE return code.
  \param s error message to print
*/
static void errno_exit(const char* s)
{
  fprintf(stderr, "%s error %d, %s\n", s, errno, strerror (errno));
  exit(EXIT_FAILURE);
}

/**
  Do ioctl and retry if error was EINTR ("A signal was caught during the ioctl() operation."). Parameters are the same as on ioctl.

  \param fd file descriptor
  \param request request
  \param argp argument
  \returns result from ioctl
*/
static int xioctl(int fd, int request, void* argp)
{
  int r;

  do r = ioctl(fd, request, argp);
  while (-1 == r && EINTR == errno);

  return r;
}

/**
  Write image to jpeg file.

  \param img image to write
*/
static void jpegWrite(unsigned char* img)
{
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;

  JSAMPROW row_pointer[1];
  FILE *outfile = fopen( jpegFilename, "wb" );

  // try to open file for saving
  if (!outfile) {
    errno_exit("jpeg");
  }

  // create jpeg data
  cinfo.err = jpeg_std_error( &jerr );
  jpeg_create_compress(&cinfo);
  jpeg_stdio_dest(&cinfo, outfile);

  // set image parameters
  cinfo.image_width = width;
  cinfo.image_height = height;
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB;

  // set jpeg compression parameters to default
  jpeg_set_defaults(&cinfo);
  // and then adjust quality setting
  jpeg_set_quality(&cinfo, jpegQuality, TRUE);

  // start compress
  jpeg_start_compress(&cinfo, TRUE);

  // feed data
  while (cinfo.next_scanline < cinfo.image_height) {
    row_pointer[0] = &img[cinfo.next_scanline * cinfo.image_width *  cinfo.input_components];
    jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }

  // finish compression
  jpeg_finish_compress(&cinfo);

  // destroy jpeg data
  jpeg_destroy_compress(&cinfo);

  // close output file
  fclose(outfile);
}

/**
  process image read
*/
static void imageProcess(const void* p)
{
  unsigned char* src = (unsigned char*)p;
  unsigned char* dst = malloc(width*height*3*sizeof(char));

  // convert from YUV422 to RGB888
  YUV422toRGB888(width,height,src,dst);

  // write jpeg
  jpegWrite(dst);
}

/**
  read single frame
*/

static int frameRead(void)
{
  struct v4l2_buffer buf;
  CLEAR (buf);

  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
  switch (errno) {

	case EAGAIN:
        return 0;

	case EIO:
        // Could ignore EIO, see spec
	// fall through

	default:
        errno_exit("VIDIOC_DQBUF");
        }
      }

  assert (buf.index < n_buffers);

  imageProcess(buffers[buf.index].start);

  if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
     errno_exit("VIDIOC_QBUF");

  return 1;
}

/**
  mainloop: read frames and process them
*/
static void mainLoop(void)
{
  unsigned int count;

  out = fopen("image.bin", "wb");
  count = 1;

  while (count-- > 0) {
    for (;;) {
      fd_set fds;
      struct timeval tv;
      int r;

      FD_ZERO(&fds);
      FD_SET(fd, &fds);

      /* Timeout. */
      tv.tv_sec = 2;
      tv.tv_usec = 0;

      r = select(fd + 1, &fds, NULL, NULL, &tv);

      if (-1 == r) {
        if (EINTR == errno)
          continue;

        errno_exit("select");
      }

      if (0 == r) {
        fprintf (stderr, "select timeout\n");
        exit(EXIT_FAILURE);
      }

      if (frameRead())
        break;

      /* EAGAIN - continue select loop. */
    }
  }
}

/**
  stop capturing
*/
static void captureStop(void)
{
  enum v4l2_buf_type type;

      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

      if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
        errno_exit("VIDIOC_STREAMOFF");
}

/**
  start capturing
*/
static void captureStart(void)
{
  unsigned int i;
  enum v4l2_buf_type type;

      for (i = 0; i < n_buffers; ++i) {
        struct v4l2_buffer buf;

        CLEAR (buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = i;

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
          errno_exit("VIDIOC_QBUF");
      }

      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

      if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
        errno_exit("VIDIOC_STREAMON");
}

static void deviceUninit(void)
{
  unsigned int i;
      for (i = 0; i < n_buffers; ++i)
      if (-1 == munmap (buffers[i].start, buffers[i].length))
        errno_exit("munmap");

  free(buffers);
}

static void mmapInit(void)
{
  struct v4l2_requestbuffers req;

  CLEAR (req);

  req.count               = 4;
  req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory              = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s does not support memory map-ping\n", deviceName);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_REQBUFS");
    }
  }

  if (req.count < 2) {
    fprintf(stderr, "Insufficient buffer memory on %s\n", deviceName);
    exit(EXIT_FAILURE);
  }

  buffers = calloc(req.count, sizeof(*buffers));

  if (!buffers) {
    fprintf(stderr, "Out of memory\n");
    exit(EXIT_FAILURE);
  }

  for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
    struct v4l2_buffer buf;

    CLEAR (buf);

    buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory      = V4L2_MEMORY_MMAP;
    buf.index       = n_buffers;

    if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
      errno_exit("VIDIOC_QUERYBUF");

    buffers[n_buffers].length = buf.length;
    buffers[n_buffers].start =
    mmap (NULL /* start anywhere */, buf.length, PROT_READ | PROT_WRITE /* required */, MAP_SHARED /* recommended */, fd, buf.m.offset);

    if (MAP_FAILED == buffers[n_buffers].start)
      errno_exit("mmap");
  }
}


/**
  initialize device
*/
static void deviceInit(void)
{
  struct v4l2_capability caps;
  struct v4l2_cropcap cropcap;
  struct v4l2_crop crop;
  struct v4l2_format fmt;
  unsigned int min;

  if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &caps)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is no V4L2 device\n",deviceName);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_QUERYCAP");
    }
  }

  if (!(caps.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
    fprintf(stderr, "%s is no video capture de-vice\n",deviceName);
    exit(EXIT_FAILURE);
  }

	printf( "Driver Caps:\n"
	" Driver: \"%s\"\n"
	" Card: \"%s\"\n"
	" Bus: \"%s\"\n"
	" Version: %d.%d\n"
	" Capabilities: %08x\n",
	caps.driver,
	caps.card,
	caps.bus_info,
	(caps.version>>16)&&0xff,
	(caps.version>>24)&&0xff,
	caps.capabilities);


  /* Select video input, video standard and tune here. */
  CLEAR(cropcap);

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect; /* reset to default */

    if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
      switch (errno) {
        case EINVAL:
          /* Cropping not supported. */
          break;
        default:
          /* Errors ignored. */
          break;
      }
    }
  } else {
    /* Errors ignored. */
  }

  printf( "Camera Cropping:\n"
  " Bounds: %dx%d+%d+%d\n"
  " Default: %dx%d+%d+%d\n"
  " Aspect: %d/%d\n",
  cropcap.bounds.width, cropcap.bounds.height,	cropcap.bounds.left, cropcap.bounds.top,
  cropcap.defrect.width, cropcap.defrect.height, crop-cap.defrect.left, cropcap.defrect.top,
  cropcap.pixelaspect.numerator, crop-cap.pixelaspect.denominator);

  struct v4l2_fmtdesc fmtdesc = {0};
  fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  char fourcc[5] = {0};
  char c, e;

  printf(" FMT : CE Desc\n--------------------\n");

  while (0 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc))
   {
	strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
	if (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG10)
		//support_grbg10 = 1;

	c = fmtdesc.flags & 1? 'C' : ' ';
	e = fmtdesc.flags & 2? 'E' : ' ';

	printf(" %s: %c%c %s\n", fourcc, c, e, fmtdesc.description);
	fmtdesc.index++;
   }

  CLEAR (fmt);

  // v4l2_format
  fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.width       = width;
  fmt.fmt.pix.height      = height;
  fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
  fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

  if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
    errno_exit("VIDIOC_S_FMT");

  /* Note VIDIOC_S_FMT may change width and height. */
  if (width != fmt.fmt.pix.width) {
    width = fmt.fmt.pix.width;
    fprintf(stderr,"Image width set to %i by device %s.\n",width,deviceName);
  }
  if (height != fmt.fmt.pix.height) {
    height = fmt.fmt.pix.height;
    fprintf(stderr,"Image height set to %i by device %s.\n",height,deviceName);
  }

  /* Buggy driver paranoia. */
  min = fmt.fmt.pix.width * 2;
  if (fmt.fmt.pix.bytesperline < min)
    fmt.fmt.pix.bytesperline = min;
  min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
  if (fmt.fmt.pix.sizeimage < min)
    fmt.fmt.pix.sizeimage = min;

  strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);
  printf( "Selected Camera Mode:\n"
  " Width: %d\n"
  " Height: %d\n"
  " PixFmt: %s\n"
  " Field: %d\n",
  fmt.fmt.pix.width,
  fmt.fmt.pix.height,
  fourcc,
  fmt.fmt.pix.field);

  mmapInit();
 }


/**
  close device
*/
static void deviceClose(void)
{
  if (-1 == close (fd))
    errno_exit("close");

  fd = -1;
}

/**
  open device
*/
static void deviceOpen(void)
{
  struct stat st;

  // stat file
  if (-1 == stat(deviceName, &st)) {
    fprintf(stderr, "Cannot identify '%s': %d, %s\n", deviceName, errno, strerror (errno));
    exit(EXIT_FAILURE);
  }

  // check if its device
  if (!S_ISCHR (st.st_mode)) {
    fprintf(stderr, "%s is no device\n", deviceName);
    exit(EXIT_FAILURE);
  }

  // open device
  fd = open(deviceName, O_RDWR /* required */ | O_NONBLOCK, 0);

  // check if opening was successfull
  if (-1 == fd) {
    fprintf(stderr, "Cannot open '%s': %d, %s\n", deviceName, errno, strerror (errno));
    exit(EXIT_FAILURE);
  }
}

/**
  print usage information
*/
static void usage(FILE* fp, int argc, char** argv)
{
  fprintf (fp,
    "Usage: %s [options]\n\n"
    "Options:\n"
    "-d | --device name   Video device name [/dev/video1]\n"
    "-h | --help          Print this message\n"
    "-o | --output        JPEG output filename\n"
    "-q | --quality       JPEG quality (0-100)\n"
    "-m | --mmap          Use memory mapped buffers\n"
    "-r | --read          Use read() calls\n"
    "-u | --userptr       Use application allocated buffers\n"
    "-W | --width         width\n"
    "-H | --height        height\n"
    "",
    argv[0]);
}

static const char short_options [] = "d:ho:q:mruW:H:";

static const struct option
long_options [] = {
        { "device",     required_argument,      NULL,           'd' },
        { "help",       no_argument,            NULL,           'h' },
        { "output",     required_argument,      NULL,           'o' },
        { "quality",    required_argument,      NULL,           'q' },
        { "mmap",       no_argument,            NULL,           'm' },
        { "read",       no_argument,            NULL,           'r' },
        { "userptr",    no_argument,            NULL,           'u' },
        { "width",      required_argument,      NULL,           'W' },
        { "height",     required_argument,      NULL,           'H' },
        { 0, 0, 0, 0 }
};

int main(int argc, char **argv)
{

	printf( "Harry Li: USB Camera Testing:\n");

  for (;;) {
    int index, c = 0;

    c = getopt_long(argc, argv, short_options, long_options, &index);

    if (-1 == c)
      break;

    switch (c) {
      case 0: /* getopt_long() flag */
        break;

      case 'd':
        deviceName = optarg;
        break;

      case 'h':
        // print help
        usage (stdout, argc, argv);
        exit(EXIT_SUCCESS);

      case 'o':
        // set jpeg filename
        jpegFilename = optarg;
        break;

      case 'q':
        // set jpeg filename
        jpegQuality = atoi(optarg);
        break;

      case 'W':
        // set width
        width = atoi(optarg);
        break;

      case 'H':
        // set height
        height = atoi(optarg);
        break;

      default:
        usage(stderr, argc, argv);
        exit(EXIT_FAILURE);
    }
  }

  // check for need parameters
  if (!jpegFilename) {
        fprintf(stderr, "You have to specify JPEG output file-name!\n\n");
        usage(stdout, argc, argv);
        exit(EXIT_FAILURE);
  }

  // open and initialize device
  deviceOpen();
  deviceInit();

  // start capturing
  captureStart();

  // process frames
  mainLoop();

  // stop capturing
  captureStop();

  // close device
  deviceUninit();
  deviceClose();

  exit(EXIT_SUCCESS);

  return 0;
}





