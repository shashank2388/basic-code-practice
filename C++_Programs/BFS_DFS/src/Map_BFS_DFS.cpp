//============================================================================
// Name        : Map_BFS_DFS.cpp
// Author      : Shashank
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;


class Graph
{
	int V;
	list<int> *adj;
	void DFSUtil(int v, bool visited[]);

public:
	Graph(int V);
	void addEdge(int u, int v);
	void BFS(int s);
	void DFS();

};

Graph :: Graph(int V)
{
	this->V = V;
	adj = new list<int> [V];
}

void Graph:: addEdge(int u, int v)
{
	adj[u]. push_back(v);
}

void Graph::BFS(int s)
{
	bool *visited = new bool[V];

	for (int i=0; i<V; i++)
		visited[i] = false;

	list<int> queue;

	visited[s] = true;
	queue.push_back(s);

	list<int>:: iterator i;

	while(!queue.empty())
	{
		s = queue.front();
		cout<< s << " ";

		queue.pop_front();

		for (i= adj[s].begin(); i!= adj[s].end(); i++)
		{
			if (!visited[*i])
			{
				visited[*i] = true;
				queue.push_back(*i);
			}
		}
	}
}


void Graph:: DFSUtil(int v, bool visited[])
{
	visited[v] = true;
	cout << v << " ";

	list<int>:: iterator i;
	for (i= adj[v].begin(); i!= adj[v].end(); i++)
	{
		if (!visited[*i])
			DFSUtil(*i, visited);
	}

}

void Graph:: DFS()
{
	bool *visited = new bool[V];
	for (int i=0; i<V; i++)
		visited[i] = false;

	for (int i = 0; i< V; i++)
		if (visited[i] == false)
			DFSUtil(i, visited);
}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
