//============================================================================
// Name        : STL-Vectors.cpp
// Author      : Shashank
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
using namespace std;


// Finds if the sum of the two elements of the vector = target and returns their index in the new vector
vector<int> twoSum(vector<int> &numbers, int target) {
    vector<int> vect;
	int diff;

	for (unsigned int index1 = 0; index1 < numbers.size(); index1++ )
	{
		diff = target - numbers.at(index1);

		for (unsigned int index2 = index1 + 1; index2 < numbers.size(); index2++ )
			if (numbers.at(index2) == diff)
			{
				vect.push_back(index1);
				vect.push_back(index2);
				return vect;
			}
	}
}


int main() {


	vector<int> MyVec;

	MyVec.push_back(5);
	MyVec.push_back(10);
	MyVec.push_back(16);
	MyVec.push_back(4);
	MyVec.push_back(8);
	MyVec.push_back(2);

	for (vector<int>:: iterator itr = MyVec.begin(); itr!= MyVec.end(); itr++)
	{
		cout << *itr << endl;
	}

	MyVec.insert(MyVec.begin() + 1, 3);

	cout << "Size of my vector is " << MyVec.size() << endl; // To get the size of the vector

	for (vector<int>:: iterator itr = MyVec.begin(); itr!= MyVec.end(); itr++)
	{
		cout << *itr << endl;
	}


	return 0;
}
