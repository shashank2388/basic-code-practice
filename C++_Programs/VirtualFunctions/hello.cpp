/*
 * hello.cpp
 *
 *  Created on: Jul 10, 2015
 *      Author: Shashank
 */

#include <iostream>
#include <string>
using namespace std;

void samechar(char*first, char*second)
{
	while (*first != '\0')
	{
		if(*first != *second)
			cout << *first;
		first++;
	}
}

class printer {
    public :
        virtual void pixel()
        {
            puts("printer pixel()");
        }
        void draw_image() {
            // Some complicated algorithm that uses our method ()
            printf("draw_image() is using : ");
            pixel();
        }
};

/* A printer model class inheriting base class */
class model_a : public printer {
    public :
        void pixel() {
            puts("model-a pixel (very sharp)");
        }
};

int main (void )
{
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
//	char str1, str2;
//	cout << "Enter string1"<< "\n";
//	cin >> str1;
//	cout << "Enter string2"<< "\n";
//	cin >> str2;
//	samechar(&str1, &str2);

    /* Let's go over simple stuff */
    model_a a;
    a.pixel(); /* Prints "model-a pixel" */

    printer p;
    p.pixel(); /* Prints "printer pixel" */

    /* Let's use a pointer of type "printer" */
    printer *ptr = NULL;
    ptr = &a;
    ptr->pixel(); /* Prints "printer pixel" */
    ptr = &p;
    ptr->pixel(); /* Prints "printer pixel" */

	return 0;
}
