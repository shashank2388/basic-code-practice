/*
 * map.cpp
 *
 *  Created on: Sep 17, 2019
 *      Author: uidm5416
 */


#include <iostream>
#include <map>
using namespace std;


int count_members(int a[], int n)
{
	map<int, int> freq;
	map<int, int>::iterator it;

	for (int i=0; i < n; i++)
	{
		freq[a[i]]++;
	}

	for (it=freq.begin(); it != freq.end(); it++)
		cout << (*it).first << " => " << (*it).second << endl;

	return 0;
}

int main () {

	int arr [] = {1, 2, 4, 2, 1, 7, 4, 4, 4, 7, 7, 4, 7, 7, 7};

	int siz_arr = sizeof(arr)/sizeof(arr[0]);
	count_members(arr, siz_arr);
	cout << "Hello World!";
	return 0;
}




