//============================================================================
// Name        : Graph.cpp
// Author      : Shashank
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <list>
using namespace std;

void addEdge(vector<int> adj[], int u, int v)
{
	adj[u].push_back(v);
	adj[v].push_back(u);

}

void addEdge_List(list<int> adj_list[], int u, int v)
{
	adj_list[u].push_back(v);
	adj_list[v].push_back(u);
}


void printGraph(list<int> adj[], int V)
{
	for (int v = 0; v < V; ++v)
	{
		cout << "\n Adjacency list of vertex "
			 << v << "\n head ";
		for (auto x : adj[v])
		   cout << "-> " << x;
		printf("\n");
	}
}

int main() {
	int V = 5;

	list<int> adj_list[V];

	vector<int> adj[V];

	addEdge_List(adj_list, 0, 1);
	addEdge_List(adj_list, 1, 2);
	printGraph(adj_list, V);


	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
