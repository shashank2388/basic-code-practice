# Basic Code Practice

##### [Preets Videos](https://www.youtube.com/c/millsinghion/videos) / Best Embedded Questions Spiral Book
##### Opamps, ADCs, PWM
##### RTOS Concepts
##### Finite State Machines
##### Computer Architecture – Caches, MMU’s, TLB’s, Paging, Virtual Memory, interrupts, DMA
##### Memory Layout for C Programs
##### Multithreaded Vs Multicores – Basic programs
##### [Write your own implementation of malloc, free](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/MallocImpementation/src/mymalloc.c)
##### Write your own implementation [memcpy](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/MemoryFunctions/src/memory.c), memmove, [circular buffer](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/Ringbuffer/src/ringbuf.c), strstr
##### Write a function to convert an int from little-endian to big-endian representation.
##### Write a program to check whether the stack grows up or down.
##### Producer-consumer thread design with synchronization

# Bit Manipulations
##### Baisc (Set a bit, test a bit, toggle a bit, test a bit)
##### [Count the number of 1s-----> using for loop and using lookup table](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/Bitwise%20Operations/src/Bitwise%20Operations.c?ref_type=heads#L85)
##### Reverse a 32 bit number (Traverse all and using lookup table)
##### [Shift d bits to left / shift d bits to right](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/Bitwise%20Operations/src/Bitwise%20Operations.c?ref_type=heads#L111) 
##### [Invert all the bits of number](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/Bitwise%20Operations/src/Bitwise%20Operations.c?ref_type=heads#L156)          
##### [Power of 2](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/Bitwise%20Operations/src/Bitwise%20Operations.c?ref_type=heads#L122)

# Arrays, Pointers

# String Type conversions
##### [ftoa](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/StringConversion/src/Conversion.c) 
##### [itoa](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/StringConversion/src/Conversion.c#L109) 
##### [atoi](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/StringConversion/src/Conversion.c#L16) 

# Basic Sorting algorithms
##### QuickSort Time optimized quicksort


# Strings:
##### Find All Anagrams in a String
##### Group Anagrams
##### Longest Substring with At Most K Distinct Characters
##### Longest Substring Without Repeating Characters
##### Add Binary
##### Valid Palindrome
##### Minimum Remove to Make Valid Parentheses
##### Add Strings
##### Valid Number
##### Verifying an Alien Dictionary
##### Next Permutation
##### Word Break
##### Simplify Path
##### Valid Palindrome II
##### Longest Palindromic Substring
##### Valid Parentheses
##### Remove Invalid Parentheses
##### Minimum Window Substring
##### Basic Calculator II

# Arrays:
##### 3Sum
##### Two Sum
##### Product of Array Except Self
##### Subarray Sum Equals K
##### Kth Largest Element in an Array
##### Intersection of Two Arrays
##### Merge Sorted Array
##### Continuous Subarray Sum
##### Maximum Sum of 3 Non-Overlapping Subarrays
##### Median of Two Sorted Arrays 
##### Search in Rotated Sorted Array -------------------------------> Binary Search
##### Find First and Last Position of Element in Sorted Array ------> Binary Search
##### Maximum Subarray
##### Search a 2D Matrix II --------> using Binary Search
##### Given an array, write a program to right shift the array  for certain steps.
##### First Missing Positive

# Data Structures:
## Linked lists: 
##### Basic Ops ([Insert](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L28), [Delete](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L127), [Reverse](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L85))
##### [Remove Nth Node From End of List](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L343)
##### [Find if linked list is circular](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L171)
##### [Middle of linked list](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L192)
##### [Merge 2 sorted linked lists / Merge k Sorted Lists](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L319)
##### [Code to find whether the linked list intersect each other and return the intersecting point](https://gitlab.com/shashank2388/basic-code-practice/-/blob/master/C_Programs/LinkedList/src/LinkedList.c#L298) 
##### Add Two Numbers represented by linked list
##### Copy List with Random Pointer
##### Convert sorted Linkedlist to Binary Search Tree 
		
# Stack:
          
# Queues: 
##### Basic Ops(enqueue, dequeue, top)
##### Queue implementation using stacks
##### Circular Queue using linked list 
##### Priority Queue 
##### Ring buffer
 
# Binary Trees: 
##### Serialize and Deserialize Binary Tree
##### Validate Binary Search Tree
##### Binary Tree Maximum Path Sum
##### Convert Binary Search Tree to Sorted Doubly Linked List
##### Vertical Order Traversal of a Binary Tree
##### Binary Tree Right Side View
##### Diameter of Binary Tree
##### Closest Binary Search Tree Value
##### Binary Search Tree Iterator 
##### All Nodes Distance K in Binary Tree
##### Binary Tree Zigzag Level Order Traversal ->Have to check with code for level order spiral
##### Reverse a binary tree
	
# Graphs: 
##### DFS 
##### BFS 
##### Topological Sort 
##### Pair of Edges Given Find circle in graph
##### Clone Graph
##### Is Graph Bipartite?
##### Critical Connections in a Network
##### Count number of islands. [In a given matrix find 1(land) surrounded by 0(water) using DFS and BFS]
		
# Hashing:
##### No questions yet

# Vector based problems    
##### K Closest Points to Origin
##### Interval List Intersections	
##### Merge Intervals
##### Meeting Rooms II
##### Meeting Rooms I
##### Task Scheduler
##### Carpooling
    
    Stock Span
    Trapping Rain Water
    
	Read N Characters Given Read4 II - Call multiple times
    
    LRU Cache
    Integer to English Words
    
    Find Median from Data Stream
    Day of the Week
    Decode ways
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Alien Dictionary
    Critical Connections in a Network
	
    
    *Happy Number
    
	*Best Time to Buy and Sell Stock 
	*Generate Parentheses

	*Coin Change
	*Spiral Matrix
	*Insert Delete GetRandom O(1)
	*Container With Most Water
	*Letter Combinations of a Phone Number
	*Word Search
	*First Missing Positive
	*Decode Ways
	*Word Ladder
	*Fraction to Recurring Decimal
	*Regular Expression Matching  
	*Reverse Integer
   
	*Permutations
	*Fizz Buzz
	Word Break
	Game of Life
	Word Break II
	Word Search II
	Accounts Merge
	Add and Search Word - Data structure design
	Expression Add Operators
	Exclusive Time of Functions
	Range Sum Query 2D - Immutable
	First Bad Version
	Goat Latin
	Monotonic Array
    
    
    Learning C++ Bo Qian ( https://www.youtube.com/channel/UCEOGtxYTB6vo6MQ-WQ9W_nQ )
    - Advanced C++
    - Modern C++ (Atleast 1st 3 videos)
    - C++ standard library
    - Rest of Modern C++ playlist
